#include "minibatchProvider.h"

MinibatchProvider::MinibatchProvider() :
    pos(0)
{
    
}

MinibatchProvider::~MinibatchProvider()
{

}

void MinibatchProvider::setMinibatch(cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &dst_layer, const cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &src)
{
    sampleset = src.copy();
    
    if (norm)
    {
        // std::cerr << "=----------------------------------------------=" << std::endl;
        // std::cerr << sampleset << std::endl;
        norm(sampleset);
        // std::cerr << sampleset << std::endl;
        // std::cerr << "=----------------------------------------------=" << std::endl;
    }

    cuv::copy(dst_layer, sampleset);
}

bool MinibatchProvider::getMinibatch(unsigned int batchsize, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &dst_layer, int id, bool test)
{
    std::cerr << "Warning: using dummy MinibatchProvider::getMinibatch" << std::endl;
}

bool MinibatchProvider::getMinibatchWithLabel(unsigned int batchsize, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &dst_layer, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &dst_label, int id, bool test)
{
    std::cerr << "Warning: using dummy MinibatchProvider::getMinibatchWithLabel" << std::endl;
}

bool MinibatchProvider::getMinibatch(unsigned int batchsize, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &dst_layer, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &hist_layer, bool realNow, int id)
{
    std::cerr << "Warning: using dummy MinibatchProvider::getMinibatch" << std::endl;
}

// ----- //

MinibatchStatistics::MinibatchStatistics(MinibatchProvider &mbp, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &act) :
    N(-1)
{
    unsigned int sid = 0;
    while (mbp.getMinibatch(act.shape()[1], act, sid))
    {
        update_stats(act);
        sid++;
    }
    finalize_stats();
}

MinibatchStatistics::~MinibatchStatistics()
{

}

void MinibatchStatistics::update_stats(cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &batch)
{
    cuv::tensor<float,cuv::dev_memory_space> local_vmin  = cuv::tensor<float,cuv::dev_memory_space>(batch.shape()[0]);
    cuv::tensor<float,cuv::dev_memory_space> local_vmax  = cuv::tensor<float,cuv::dev_memory_space>(batch.shape()[0]);
    cuv::tensor<float,cuv::dev_memory_space> local_mean  = cuv::tensor<float,cuv::dev_memory_space>(batch.shape()[0]);
    cuv::tensor<float,cuv::dev_memory_space> local_mean2 = cuv::tensor<float,cuv::dev_memory_space>(batch.shape()[0]);

    cuv::fill(local_mean, 0);
    cuv::fill(local_mean2, 0);

    cuv::reduce_to_col(local_mean,batch);
    cuv::reduce_to_col(local_mean2,batch,cuv::RF_ADD_SQUARED);
    cuv::reduce_to_col(local_vmin,batch,cuv::RF_MIN);
    cuv::reduce_to_col(local_vmax,batch,cuv::RF_MAX);

    if (N==-1)
    {
        N = batch.shape()[1];
        mean = local_mean;
        mean2 = local_mean2;
        min = local_vmin;
        max = local_vmax;

    }
    else
    {
        N += batch.shape()[1];
        cuv::apply_binary_functor(mean, local_mean, cuv::BF_ADD);
        cuv::apply_binary_functor(mean2, local_mean2, cuv::BF_ADD);
        cuv::apply_binary_functor(min, local_vmin, cuv::BF_MIN);
        cuv::apply_binary_functor(max, local_vmax, cuv::BF_MAX);
        local_mean.dealloc();
        local_mean2.dealloc();
        local_vmin.dealloc();
        local_vmax.dealloc();
    }
}

void MinibatchStatistics::finalize_stats()
{
    // use N, mean and mean2 to generate data for normalization

    // mean := (mean/N)^2
    cuv::apply_scalar_functor(mean,cuv::SF_MULT, 1./N);
    cuv::tensor<float,cuv::dev_memory_space> sqmean = mean.copy();
    cuv::apply_scalar_functor(sqmean, cuv::SF_SQUARE);

    // mean2 -= mean2/n - squared_mean
    cuv::apply_scalar_functor(mean2, cuv::SF_MULT, 1./N);
    cuv::apply_binary_functor(mean2, sqmean, cuv::BF_SUBTRACT);

    // std is sqrt of difference
    cuv::apply_scalar_functor(mean2, cuv::SF_ADD, 0.01); // numerical stability
    cuv::apply_scalar_functor(mean2, cuv::SF_SQRT);
    std = mean2;
    sqmean.dealloc();

    // negate mean (so we can add it to normalize a matrix)
    negative_mean = cuv::tensor<float,cuv::dev_memory_space>(mean.shape());
    cuv::copy(negative_mean, mean);
    cuv::apply_scalar_functor(negative_mean,cuv::SF_MULT, -1.);

    // calculate range
    cuv::apply_binary_functor(max, min, cuv::BF_SUBTRACT);
    cuv::apply_scalar_functor(max, cuv::SF_MAX, 1.);
    range = max;

    // calculate negative min
    negative_min = cuv::tensor<float,cuv::dev_memory_space>(min.shape());
    cuv::apply_scalar_functor(range, cuv::SF_ADD, 0.01); // numerical stability
    cuv::copy(negative_min, min);
    cuv::apply_scalar_functor(negative_min, cuv::SF_MULT, -1.);

    assert(!cuv::has_nan(mean));
    assert(!cuv::has_inf(mean));
    assert(!cuv::has_nan(negative_mean));
    assert(!cuv::has_inf(negative_mean));
    assert(!cuv::has_nan(std));
    assert(!cuv::has_inf(std));
    assert(!cuv::has_nan(min));
    assert(!cuv::has_nan(negative_min));
    assert(!cuv::has_inf(range));
}

void MinibatchStatistics::normalize_zmuv(cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &batch)
{
    cuv::matrix_plus_col(batch, negative_mean);
    cuv::matrix_divide_col(batch, std);
}

void MinibatchStatistics::normalize_255(cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &batch)
{
    cuv::apply_scalar_functor(batch, cuv::SF_DIV, 255.);
}

void MinibatchStatistics::normalize_180deg(cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &batch)
{
    cuv::apply_scalar_functor(batch, cuv::SF_ADD, 180.);
    cuv::apply_scalar_functor(batch, cuv::SF_DIV, 360.);
}

void MinibatchStatistics::normalize_minmax(cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &batch)
{
    cuv::matrix_plus_col(batch, negative_min);
    cuv::matrix_divide_col(batch, range);
}

void MinibatchStatistics::denormalize_zmuv(cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &batch)
{
    cuv::matrix_times_col(batch, std);
    cuv::matrix_plus_col(batch, mean);
    // cuv::matrix_plus_col(batch, negative_mean);
    // cuv::matrix_divide_col(batch, std);
}

void MinibatchStatistics::denormalize_255(cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &batch)
{
    cuv::apply_scalar_functor(batch, cuv::SF_MULT, 255.);
}

void MinibatchStatistics::denormalize_180deg(cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &batch)
{
    cuv::apply_scalar_functor(batch, cuv::SF_MULT, 360.);
    cuv::apply_scalar_functor(batch, cuv::SF_ADD, -180.);
}

void MinibatchStatistics::denormalize_minmax(cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &batch)
{
    cuv::matrix_times_col(batch,range);
    cuv::matrix_plus_col(batch, min);
    // cuv::matrix_plus_col(batch, negative_min);
    // cuv::matrix_divide_col(batch, range);
}

// ----- //

ListMinibatchProvider::ListMinibatchProvider() : MinibatchProvider()
{
    
}

ListMinibatchProvider::ListMinibatchProvider(std::vector<cuv::tensor<float,cuv::host_memory_space,cuv::column_major> > &list) : MinibatchProvider()
{
    dataset = list;
}

ListMinibatchProvider::ListMinibatchProvider(std::vector<cuv::tensor<float,cuv::host_memory_space,cuv::column_major> > &list, std::vector<cuv::tensor<float,cuv::host_memory_space,cuv::column_major> > &list_test) : MinibatchProvider()
{
    dataset = list;
    test_dataset = list_test;
}

ListMinibatchProvider::~ListMinibatchProvider()
{

}

bool ListMinibatchProvider::getMinibatch(unsigned int batchsize, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &dst_layer, int id, bool test)
{
    std::vector<cuv::tensor<float,cuv::host_memory_space,cuv::column_major> > data;
    if (test)
    {
        data = test_dataset;
    }
    else
    {
        data = dataset;
    }

    //assumes data is in columns (several columns = different samples)
    if (id < 0)
    {
        id = pos;
        pos += 1;
        pos %= data.size(); // number of samples
    }
    if (id >= data.size())
    {
        return false;
    }

    //dst_layer = dataset[id]; // copy happens (host -> device)

    setMinibatch(dst_layer, data[id]);

    return true;
}

bool ListMinibatchProvider::getMinibatchWithLabel(unsigned int batchsize, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &dst_layer, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &dst_label, int id, bool test)
{
    std::vector<cuv::tensor<float,cuv::host_memory_space,cuv::column_major> > data;
    std::vector<cuv::tensor<float,cuv::host_memory_space,cuv::column_major> > labels;
    if (test)
    {
        data = test_dataset;
        labels = test_labelset;
    }
    else
    {
        data = dataset;
        labels = labelset;
    }

    //assumes data is in columns (several columns = different samples)
    if (id < 0)
    {
        id = pos;
        pos += 1;
        pos %= data.size(); // number of samples
    }
    if (id >= data.size())
    {
        return false;
    }

    //dst_layer = dataset[id]; // copy happens (host -> device)

    setMinibatch(dst_layer, data[id]);

    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> devLabels = labels[id];
    cuv::copy(dst_label, devLabels);


    return true;
}

void ListMinibatchProvider::setLabels(std::vector<cuv::tensor<float,cuv::host_memory_space,cuv::column_major> >& labels)
{
    labelset = labels;
}

void ListMinibatchProvider::setTestLabels(std::vector<cuv::tensor<float,cuv::host_memory_space,cuv::column_major> >& testLabels)
{
    test_labelset = testLabels;
}
