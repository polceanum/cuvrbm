#include "nodeLayer.h"

NodeLayer::NodeLayer(unsigned int size, unsigned int batchSize, UnitType type):
size(size),
_batchSize(batchSize),
unitType(type)
{
    alloc();
}

NodeLayer::~NodeLayer()
{

}

void NodeLayer::sample()
{
    if (unitType == UnitType::gaussian || unitType == UnitType::cont)
    {
        cuv::add_rnd_normal(act);
    }
    else if (unitType == UnitType::binary)
    {
        cuv::rnd_binarize(act);
    }
}

void NodeLayer::nonlinearity()
{
    if (unitType != UnitType::gaussian)
    {
        cuv::apply_scalar_functor(act, cuv::SF_SIGM);
    }
}

void NodeLayer::switchToPChain()
{
    _org = act;
    act = pChain;
}

void NodeLayer::switchToOrg()
{
    pChain = act;
    act = _org;
}

void NodeLayer::postUpdate(bool sample)
{
    nonlinearity();
    if (sample)
    {
        this->sample();
    }
}

// --- gpu memory management --- //

void NodeLayer::alloc()
{
    act = cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>(size, _batchSize);
    cuv::fill(act, 0);
}


void NodeLayer::dealloc()
{
    act.dealloc();
}

void NodeLayer::allocPChain()
{
    pChain = cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>(size, _batchSize);
    cuv::fill(pChain, 0);
}

void NodeLayer::deallocPChain()
{
    pChain.dealloc();
}