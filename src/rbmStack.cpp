#include "rbmStack.h"

RBMStack::RBMStack(const Configuration &cfg) :
    _cfg(cfg)
{
    for (unsigned int i=0; i<cfg.num_layers; ++i)
    {
        _layers.push_back(NodeLayer(cfg.l_size.at(i), cfg.batchsize, cfg.utype.at(i)));
        if (cfg.cd_type == CDType::pcd)
        {
            _layers[i].allocPChain();
        }
    }

    for (unsigned int i=0; i<cfg.num_layers-1; ++i)
    {
        _weights.push_back(WeightLayer(_layers[i], _layers[i+1], cfg, i));
    }
}

RBMStack::~RBMStack()
{

}

float RBMStack::getErr(unsigned int layerNum, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &originalData)
{
    cuv::apply_binary_functor(_layers[layerNum].act, originalData, cuv::BF_SUBTRACT);
    float sqerr = pow(cuv::norm2(_layers[layerNum].act),2);
    return sqerr/(_layers[layerNum].size*_cfg.batchsize);
}

float RBMStack::getLearnrate(unsigned int iter, unsigned int iterMax)
{
    if (_cfg.learnRate_sched == LearnRateSchedule::linear)
    {
        return _cfg.learnRate * (1 - (float)iter/iterMax);
    }
    else if (_cfg.learnRate_sched == LearnRateSchedule::exponential)
    {
        return _cfg.learnRate * exp(-10 * (float)iter/iterMax);
    }
    else if (_cfg.learnRate_sched == LearnRateSchedule::divide)
    {
        unsigned int startAt = 10000;
        if (iter < startAt)
        {
            return _cfg.learnRate;
        }
        else
        {
            return _cfg.learnRate * 1000.0f / (1000.0f + iter - startAt);
        }
    }

    // else (static)
    return _cfg.learnRate;
}

void RBMStack::upPass(unsigned int layerNum, bool sample)
{
    _weights[layerNum].upPass(_layers[layerNum], _layers[layerNum+1], sample);
}

void RBMStack::upPassDrop(unsigned int layerNum, bool sample, bool pcdPass)
{
    _weights[layerNum].upPassDrop(_layers[layerNum], _layers[layerNum+1], sample, pcdPass);
}

void RBMStack::downPass(unsigned int layerNum, bool sample)
{
    _weights[layerNum-1].downPass(_layers[layerNum-1], _layers[layerNum], sample);
}

void RBMStack::updateLayer(unsigned int layerNum, bool sample)
{
    NodeLayer &L = _layers[layerNum];

    if (layerNum == 0)
    {
        downPass(layerNum+1, sample);
    }
    if (layerNum == _layers.size()-1)
    {
        upPass(layerNum-1, sample);
    }
    if ((layerNum < _layers.size()-1) && (layerNum>0))
    {
        NodeLayer &hi = _layers[layerNum+1];
        NodeLayer &lo = _layers[layerNum-1];
        WeightLayer &wlo = _weights[layerNum-1];
        WeightLayer &whi = _weights[layerNum];

        cuv::prod(L.act, whi.mat, hi.act, 'n', 'n');
        cuv::matrix_plus_col(L.act, whi.bias_lo);

        cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> tmp = cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>(L.act.shape());
        cuv::copy(tmp, L.act);
        cuv::prod(L.act, wlo.mat, lo.act, 't', 'n');
        cuv::matrix_plus_col(L.act, wlo.bias_hi);

        // add parts from above/below
        cuv::apply_binary_functor(L.act, tmp, cuv::BF_AXPBY, 0.5, 0.5); // computes x = a * x + b * y
        tmp.dealloc();

        L.nonlinearity();

        if (sample)
        {
            L.sample();
        }
    }
}

void RBMStack::pcdStep(unsigned int layerNum)
{
    _layers[layerNum].switchToPChain();
    _layers[layerNum+1].switchToPChain();

    if (_cfg.use_dropout || _cfg.use_experimental_sf)
    {
        upPassDrop(layerNum, true, true);
    }
    else
    {
        upPass(layerNum, true);
    }
    downPass(layerNum+1, true);
    _weights[layerNum].updateGradientNeg(_layers[layerNum], _layers[layerNum+1], _cfg.batchsize);
    
    _layers[layerNum].switchToOrg();
    _layers[layerNum+1].switchToOrg();
}

void RBMStack::cdnStep(unsigned int layerNum)
{
    for (unsigned int k=0; k<_cfg.kSteps; ++k)
    {
        if (_cfg.use_dropout || _cfg.use_experimental_sf)
        {
            upPassDrop(layerNum, true, true);
        }
        else
        {
            upPass(layerNum, true);
        }
        downPass(layerNum+1, true);
    }

    if (_cfg.use_dropout || _cfg.use_experimental_sf)
    {
        upPassDrop(layerNum, true, true);
    }
    else
    {
        upPass(layerNum, true);
    }
    _weights[layerNum].updateGradientNeg(_layers[layerNum], _layers[layerNum+1], _cfg.batchsize);
}

void RBMStack::trainLayer(MinibatchProvider &mbp, unsigned int iterStart, unsigned int iterMax, unsigned int layerNum)
{
    if (_cfg.cd_type == CDType::pcd)
    {
        mbp.getMinibatch(_cfg.batchsize, _layers[layerNum].pChain);
    }

    _weights[layerNum].allocUpdateMatrix();

    for (unsigned int iter = iterStart; iter<iterMax; ++iter)
    {
        _current_iter = iter;

        // new learnrate if schedule
        float learnRate = getLearnrate(iter, iterMax);

        // positive phase
        mbp.getMinibatch(_cfg.batchsize, _layers[layerNum].act);
        if (_cfg.use_dropout || _cfg.use_experimental_sf)
        {
            upPassDrop(layerNum, true, false);
        }
        else
        {
            upPass(layerNum, true);
        }
        _weights[layerNum].updateGradientPos(_layers[layerNum], _layers[layerNum+1]);

        // negative phase
        if (_cfg.cd_type == CDType::cdn)
        {
            cdnStep(layerNum);
        }
        else if (_cfg.cd_type == CDType::pcd)
        {
            pcdStep(layerNum);
        }

        //update weights and biases
        _weights[layerNum].updateStep(learnRate, _cfg.cost);

        if ((iter != 0) && (iter%100==0))
        {
            printError(layerNum, iter, mbp);

            //debug
            if (_iter_callback)
            {
                _iter_callback(layerNum);
            }
        }
    }

    // if (_cfg.use_dropout)
    // {
    //     cuv::apply_scalar_functor(_weights[layerNum].mat, cuv::SF_MULT, 0.5);
    // }

    _weights[layerNum].deallocUpdateMatrix();
}

void RBMStack::run(unsigned int iterStart, unsigned int iterMax, MinibatchProvider &mbp)
{
    // Trains all levels of the RBM stack for itermax epochs. Lowest-level data comes from mbatch_provider.
    MinibatchProvider *minibatchProvider = &mbp;
    bool destroyLastProvider = false;

    for (unsigned int layer = 0; layer < _cfg.num_layers-1; ++layer)
    {
        if (layer+1 >= _cfg.continue_learning)
        {
            trainLayer(*minibatchProvider, iterStart, iterMax, layer);
        }

        if (layer < _cfg.num_layers-2)
        {
            MinibatchProvider *tmp = getHiddenRep(layer, minibatchProvider);
            if (destroyLastProvider) delete minibatchProvider;
            minibatchProvider = tmp;
            destroyLastProvider = true;
        }
    }

    if (destroyLastProvider) delete minibatchProvider;
}

MinibatchProvider* RBMStack::getHiddenRep(unsigned int layerNum, MinibatchProvider *mbp, bool passTestset)
{
    unsigned int id = 0;
    std::vector<cuv::tensor<float,cuv::host_memory_space,cuv::column_major> > mblist;

    while (mbp->getMinibatch(_cfg.batchsize, _layers[layerNum].act, id))
    {
        upPass(layerNum, false);
        mblist.push_back(_layers[layerNum+1].act); // copy happens (device->host)
        id++;
    }

    if (passTestset)
    {
        id = 0;
        std::vector<cuv::tensor<float,cuv::host_memory_space,cuv::column_major> > mblist_test;

        while (mbp->getMinibatch(_cfg.batchsize, _layers[layerNum].act, id, true))
        {
            upPass(layerNum, false);
            mblist_test.push_back(_layers[layerNum+1].act); // copy happens (device->host)
            id++;
        }

        return new ListMinibatchProvider(mblist, mblist_test);
    }

    return new ListMinibatchProvider(mblist);
}

void RBMStack::generateDataset(ListMinibatchProvider& dst, MinibatchProvider& src, int label_size, bool passTestset)
{
    assert(_cfg.num_layers > 1); // 1 layer means no weights, so nothing to generate

    MinibatchProvider *minibatchProvider = &src;
    bool destroyLastProvider = false;

    for (unsigned int layer = 0; layer < _cfg.num_layers-1; ++layer)
    {
        MinibatchProvider *tmp = getHiddenRep(layer, minibatchProvider, passTestset);
        if (destroyLastProvider) delete minibatchProvider;
        minibatchProvider = tmp;
        destroyLastProvider = true;
    }

    if (label_size > 0)
    {
        unsigned int id = 0;
        std::vector<cuv::tensor<float,cuv::host_memory_space,cuv::column_major> > mblist_labels;
        cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> dummyAct = cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>(_layers[0].act.shape());
        cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> labels = cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>(label_size, _cfg.batchsize);
        while (src.getMinibatchWithLabel(_cfg.batchsize, dummyAct, labels, id))
        {
            mblist_labels.push_back(labels);
            id++;
        }

        ((ListMinibatchProvider*)minibatchProvider)->setLabels(mblist_labels);

        if (passTestset)
        {
            id = 0;
            std::vector<cuv::tensor<float,cuv::host_memory_space,cuv::column_major> > mblist_test_labels;
            cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> dummyTestAct = cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>(_layers[0].act.shape(0), _cfg.batchsize);
            cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> testLabels = cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>(label_size, _cfg.batchsize);
            while (src.getMinibatchWithLabel(_cfg.batchsize, dummyTestAct, testLabels, id, true))
            {
                mblist_test_labels.push_back(testLabels);
                id++;
            }

            ((ListMinibatchProvider*)minibatchProvider)->setTestLabels(mblist_test_labels);
        }
    }

    dst = *((ListMinibatchProvider*)minibatchProvider);

    if (destroyLastProvider) delete minibatchProvider;
}

void RBMStack::resetPChain(MinibatchProvider &mbp)
{
    mbp.getMinibatch(_cfg.batchsize, _layers[0].pChain);
    for (unsigned int i=0; i<_layers.size(); ++i)
    {
        _layers[i].switchToPChain();
    }
    for (unsigned int i=0; i<_weights.size(); ++i)
    {
        upPass(i, true);
    }
    for (unsigned int i=0; i<_layers.size(); ++i)
    {
        _layers[i].switchToOrg();
    }
}

void RBMStack::printError(unsigned int layerNum, unsigned int iter, MinibatchProvider &mbp)
{
    if (layerNum == 0) mbp.getMinibatch(_cfg.batchsize, _layers[layerNum].act, -1, true);
    else mbp.getMinibatch(_cfg.batchsize, _layers[layerNum].act, -1); // TODO test data is not passed up during training...
    
    upPass(layerNum, false);
    downPass(layerNum+1, false);

    for (unsigned int l=layerNum; l>0; --l)
    {
        downPass(l, false);
    }

    std::chrono::high_resolution_clock::time_point rightNow = std::chrono::high_resolution_clock::now();
    std::chrono::duration<float> time_span = std::chrono::duration_cast<std::chrono::duration<double>>(rightNow - last_err_time);

    float err = getErr(layerNum, mbp.sampleset);
    float n = cuv::norm2(_weights[layerNum].mat);
    float timeSpent = time_span.count();
    std::cerr << "Iter: " << iter
              << " Err: " << err
              << " |W| = " << n
              << " -- "
              << " timespent: " << timeSpent << " -- "
              << (timeSpent/(_cfg.batchsize*(iter-last_err_time_iter))) << "s/img "
              << ((_cfg.batchsize*(iter-last_err_time_iter))/timeSpent) << "img/s"
              << std::endl;

    last_err_time = rightNow;
    last_err_time_iter = iter;
}

//serialization
void RBMStack::saveLayer(unsigned int layerNum, const std::string &prefix, const std::string &suffix)
{

}

void RBMStack::loadLayer(unsigned int layerNum, const std::string &prefix, const std::string &suffix)
{

}

//debug
void RBMStack::setIterationCallback(std::function<void (unsigned int)> cb)
{
    _iter_callback = cb;
}