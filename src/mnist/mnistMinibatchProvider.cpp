#include "mnist/mnistMinibatchProvider.h"

MNISTMinibatchProvider::MNISTMinibatchProvider(Dataset &mnist_data) : MinibatchProvider()
{
    dataset = mnist_data.data;
    test_dataset = mnist_data.test_data;
    labelset = mnist_data.data_labels;
    test_labelset = mnist_data.test_labels;
}

MNISTMinibatchProvider::~MNISTMinibatchProvider()
{

}

bool MNISTMinibatchProvider::getMinibatch(unsigned int batchsize, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &dst_layer, int id, bool test)
{
    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> data;
    if (test)
    {
        data = test_dataset;
    }
    else
    {
        data = dataset;
    }
    //assumes data is in columns (several columns = different samples)
    if (id < 0)
    {
        pos += batchsize;
        pos %= data.shape()[1]; // number of samples
        if (data.shape()[1] < pos+batchsize-1)
        {
            pos = 0;
        }
        id = pos;
    }
    else
    {
        id = id*batchsize;
    }

    if (data.shape()[1] < id+batchsize)
    {
        return false;
    }

    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> minibatch(cuv::indices[cuv::index_range(0,data.shape()[0])][cuv::index_range(0,batchsize)], data.ptr()+id*data.shape()[0]);
    
    setMinibatch(dst_layer, minibatch);

    return true;
}

bool MNISTMinibatchProvider::getMinibatchWithLabel(unsigned int batchsize, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &dst_layer, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &dst_label, int id, bool test)
{
    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> data;
    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> labels;
    if (test)
    {
        data = test_dataset;
        labels = test_labelset;
    }
    else
    {
        data = dataset;
        labels = labelset;
    }
    //assumes data is in columns (several columns = different samples)
    if (id < 0)
    {
        pos += batchsize;
        pos %= data.shape()[1]; // number of samples
        if (data.shape()[1] < pos+batchsize-1)
        {
            pos = 0;
        }
        id = pos;
    }
    else
    {
        id = id*batchsize;
    }

    if (data.shape()[1] < id+batchsize)
    {
        return false;
    }

    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> minibatch(cuv::indices[cuv::index_range(0,data.shape()[0])][cuv::index_range(0,batchsize)], data.ptr()+id*data.shape()[0]);
    
    setMinibatch(dst_layer, minibatch);

    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> minibatchLabels(cuv::indices[cuv::index_range(0,labels.shape()[0])][cuv::index_range(0,batchsize)], labels.ptr()+id*labels.shape()[0]);

    cuv::copy(dst_label, minibatchLabels);

    return true;
}