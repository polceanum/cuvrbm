#include "mnist/mnistData.h"

#include <iostream>
#include <fstream>

MNISTData::MNISTData(Configuration &cfg, const std::string path) : Dataset(),
    normalized(false)
{
    cfg.px = 28;
    cfg.py = 28;
    cfg.test_batchsize = 10000; //16*25;
    cfg.maps_bottom = 1;

    if (cfg.batchsize == -1)
    {
        cfg.batchsize = 16*25;
    }

    ReadMNIST(path+"/train-images.idx3-ubyte", data);
    ReadMNIST(path+"/t10k-images.idx3-ubyte", test_data);
    ReadMNIST(path+"/train-labels.idx1-ubyte", data_labels);
    ReadMNIST(path+"/t10k-labels.idx1-ubyte", test_labels);

    data_labels = labelMatrix(data_labels, 10);
    test_labels = labelMatrix(test_labels, 10);
}

MNISTData::~MNISTData()
{

}

int MNISTData::ReverseInt(int i)
{
    unsigned char ch1, ch2, ch3, ch4;
    ch1 = i & 255;
    ch2 = (i >> 8) & 255;
    ch3 = (i >> 16) & 255;
    ch4 = (i >> 24) & 255;
    return((int)ch1 << 24) + ((int)ch2 << 16) + ((int)ch3 << 8) + ch4;
}

void MNISTData::ReadMNIST(const std::string filename, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &data)
{
    std::ifstream file(filename.c_str(), std::ios::binary);
    if (file.is_open())
    {
        int magic_number = 0;
        int number_of_images = 0;
        int n_rows = 0;
        int n_cols = 0;
        file.read((char*)&magic_number, sizeof(magic_number));
        magic_number = ReverseInt(magic_number);
        std::cerr << "magic_number = " << magic_number << std::endl;
        file.read((char*)&number_of_images, sizeof(number_of_images));
        number_of_images = ReverseInt(number_of_images);
        std::cerr << "number_of_images = " << number_of_images << std::endl;
        if (magic_number == 2051)
        {
            file.read((char*)&n_rows, sizeof(n_rows));
            n_rows = ReverseInt(n_rows);
            std::cerr << "n_rows = " << n_rows << std::endl;
            file.read((char*)&n_cols, sizeof(n_cols));
            n_cols = ReverseInt(n_cols);
            std::cerr << "n_cols = " << n_cols << std::endl;
        }
        else
        {
            n_rows = 1;
            n_cols = 1;
        }
        

        cuv::tensor<float,cuv::host_memory_space,cuv::column_major> host_data = cuv::tensor<float,cuv::host_memory_space,cuv::column_major>(n_rows*n_cols, number_of_images);

        std::cerr << "Loading data..." << std::endl;
        for (int i = 0; i<number_of_images; ++i)
        {
            for (int r = 0; r<n_rows; ++r)
            {
                for (int c = 0; c<n_cols; ++c)
                {
                    unsigned char temp = 0;
                    file.read((char*)&temp, sizeof(temp));
                    if (magic_number == 2051)
                    {
                        host_data((n_rows*r) + c, i) = (double)temp;
                    }
                    else
                    {
                        host_data((n_rows*r) + c, i) = (int)temp;
                    }
                }
            }
        }

        std::cerr << "Transfering to device... ";
        data = host_data;
        std::cerr << "done" << std::endl;
    }
    else
    {
        std::cerr << "File not found !" << std::endl;
    }
}

cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> MNISTData::labelMatrix(const cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>& labels, int size)
{
    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> result = cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>(size, labels.shape(1));
    cuv::fill(result, 0);
    cuv::tensor<float,cuv::host_memory_space,cuv::column_major> hostLabels = labels;
    for (int i=0; i<hostLabels.shape(1); ++i)
    {
        result(hostLabels(0,i), i) = 1;
    }
    
    return result;
}