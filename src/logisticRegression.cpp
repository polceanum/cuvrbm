#include "logisticRegression.h"

#include <cuv/tensor_ops/rprop.hpp>

#include <iostream>
#include <string>
#include <math.h>

using namespace std;


LogisticRegression::LogisticRegression(const Configuration &cfg) : //int in, int out, int batchsize)
    _cfg(cfg)
{
    assert(cfg.num_layers == 2);

    n_in = _cfg.l_size.at(0);
    n_out = _cfg.l_size.at(1);
    batch_size = _cfg.batchsize;

    W = cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>(n_in, n_out);
    cuv::fill(W, 0);
    b = cuv::tensor<float,cuv::dev_memory_space>(n_out);
    cuv::fill(b, 0);
}

LogisticRegression::~LogisticRegression()
{
}


void LogisticRegression::train(const cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>& x, const cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>& y, double lr)
{
    predict(x, p_y_given_x);

    cuv::prod(tmp_W, x, p_y_given_x, 'n', 't');
    cuv::prod(tmp_W, x, y, 'n', 't', 1.f/batch_size, -1.f/batch_size);

    cuv::reduce_to_col(tmp_b, p_y_given_x);
    cuv::reduce_to_col(tmp_b, y, cuv::RF_ADD, 1.f/batch_size, -1.f/batch_size);

    cuv::learn_step_weight_decay(W, tmp_W, -lr);
    cuv::learn_step_weight_decay(b, tmp_b, -lr);
}

void LogisticRegression::predict(const cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>& x, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>& y)
{
    cuv::prod(y, W, x, 't', 'n');
    cuv::matrix_plus_col(y, b);

    softmax(y);
}

void LogisticRegression::run(unsigned int iterStart, unsigned int iterMax, MinibatchProvider &mbp)
{
    allocUpdateMatrix();

    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> minibatch_x = cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>(n_in, batch_size);
    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> minibatch_y = cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>(n_out, batch_size);

    for (unsigned int iter = iterStart; iter<iterMax; ++iter)
    {
        mbp.getMinibatchWithLabel(_cfg.batchsize, minibatch_x, minibatch_y);

        train(minibatch_x, minibatch_y, _cfg.learnRate);

        if ((iter != 0) && (iter%100==0))
        {
            printError(iter, mbp);

            //debug
            if (_iter_callback)
            {
                _iter_callback();
            }
        }
    }

    deallocUpdateMatrix();
}

void LogisticRegression::softmax(cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>& dst)
{
    cuv::tensor<float,cuv::dev_memory_space> red(dst.shape(1));
    cuv::reduce_to_row(red, dst, cuv::RF_LOGADDEXP, -1.f);

    cuv::matrix_plus_row(dst, red);

    cuv::apply_scalar_functor(dst, cuv::SF_EXP);
}

void LogisticRegression::allocUpdateMatrix()
{
    p_y_given_x = cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>(n_out, batch_size);
    cuv::fill(p_y_given_x, 0);
    tmp_W = cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>(n_in, n_out);
    cuv::fill(tmp_W, 0);
    tmp_b = cuv::tensor<float,cuv::dev_memory_space>(n_out);
    cuv::fill(tmp_b, 0);
}

void LogisticRegression::deallocUpdateMatrix()
{
    cuv::safeThreadSync();
    p_y_given_x.dealloc();
    tmp_W.dealloc();
    tmp_b.dealloc();
}

void LogisticRegression::printError(unsigned int iter, MinibatchProvider &mbp)
{
    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> minibatch_x = cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>(n_in, _cfg.test_batchsize);
    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> minibatch_y = cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>(n_out, _cfg.test_batchsize);
    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> prediction = cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>(n_out, _cfg.test_batchsize);
    
    double total_test_loss = 0.0;
    double divBy = 0;
    unsigned int id = 0;

    while (mbp.getMinibatchWithLabel(_cfg.test_batchsize, minibatch_x, minibatch_y, id, true))
    {
        double this_test_loss = 0.0;

        predict(minibatch_x, prediction);

        cuv::tensor<float,cuv::dev_memory_space> predLabel = cuv::tensor<float,cuv::dev_memory_space>(_cfg.test_batchsize);
        cuv::reduce_to_row(predLabel, prediction, cuv::RF_ARGMAX);
        cuv::tensor<float,cuv::dev_memory_space> realLabel = cuv::tensor<float,cuv::dev_memory_space>(_cfg.test_batchsize);
        cuv::reduce_to_row(realLabel, minibatch_y, cuv::RF_ARGMAX);

        cuv::tensor<float,cuv::dev_memory_space> equals = cuv::tensor<float,cuv::dev_memory_space>(_cfg.test_batchsize);
        cuv::apply_binary_functor(equals, predLabel, realLabel, cuv::BF_EQ);

        this_test_loss = (_cfg.test_batchsize - cuv::sum(equals));

        this_test_loss /= _cfg.test_batchsize;

        id++;
        total_test_loss += this_test_loss;
        divBy += 1;
    }

    total_test_loss /= divBy;

    std::chrono::high_resolution_clock::time_point rightNow = std::chrono::high_resolution_clock::now();
    std::chrono::duration<float> time_span = std::chrono::duration_cast<std::chrono::duration<double>>(rightNow - last_err_time);

    float n = cuv::norm2(W);
    float timeSpent = time_span.count();
    std::cerr << "Iter: " << iter
              << " Err: " << total_test_loss
              << " |W| = " << n
              << " -- "
              << " timespent: " << timeSpent << " -- "
              << (timeSpent/(_cfg.batchsize*(iter-last_err_time_iter))) << "s/img "
              << ((_cfg.batchsize*(iter-last_err_time_iter))/timeSpent) << "img/s"
              << std::endl;

    last_err_time = rightNow;
    last_err_time_iter = iter;
}

//debug
void LogisticRegression::setIterationCallback(std::function<void (void)> cb)
{
    _iter_callback = cb;
}