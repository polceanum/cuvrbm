#include "crbmStack.h"

CRBMStack::CRBMStack(const Configuration &cfg, unsigned int delay) :
    _cfg(cfg)
{
    for (unsigned int i=0; i<cfg.num_layers; ++i)
    {
        _layers.push_back(NodeLayer(cfg.l_size.at(i), cfg.batchsize, cfg.utype.at(i)));
        // if (cfg.cd_type == CDType::pcd)
        // {
        //     _layers[i].allocPChain();
        // }
    }

    for (unsigned int i=0; i<cfg.num_layers-1; ++i)
    {
        _weights.push_back(ConditionalWeightLayer(_layers[i], _layers[i+1], cfg, i, delay));
    }
}

CRBMStack::~CRBMStack()
{

}

float CRBMStack::getErr(unsigned int layerNum, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &originalData)
{
    cuv::apply_binary_functor(_layers[layerNum].act, originalData, cuv::BF_SUBTRACT);
    float sqerr = pow(cuv::norm2(_layers[layerNum].act),2);
    return sqerr/(_layers[layerNum].size*_cfg.batchsize);
}

float CRBMStack::getLearnrate(unsigned int iter, unsigned int iterMax)
{
    if (_cfg.learnRate_sched == LearnRateSchedule::linear)
    {
        return _cfg.learnRate * (1 - (float)iter/iterMax);
    }
    else if (_cfg.learnRate_sched == LearnRateSchedule::exponential)
    {
        return _cfg.learnRate * exp(-10 * (float)iter/iterMax);
    }
    else if (_cfg.learnRate_sched == LearnRateSchedule::divide)
    {
        unsigned int startAt = 10000;
        if (iter < startAt)
        {
            return _cfg.learnRate;
        }
        else
        {
            return _cfg.learnRate * 1000.0f / (1000.0f + iter - startAt);
        }
    }

    // else (static)
    return _cfg.learnRate;
}

void CRBMStack::upPass(unsigned int layerNum, bool sample)
{
    _weights[layerNum].upPass(_layers[layerNum], _layers[layerNum+1], sample);
}

void CRBMStack::downPass(unsigned int layerNum, bool sample)
{
    _weights[layerNum-1].downPass(_layers[layerNum-1], _layers[layerNum], sample);
}

void CRBMStack::updateLayer(unsigned int layerNum, bool sample)
{
    NodeLayer &L = _layers[layerNum];

    if (layerNum == 0)
    {
        downPass(layerNum+1, sample);
    }
    if (layerNum == _layers.size()-1)
    {
        upPass(layerNum-1, sample);
    }
    if ((layerNum < _layers.size()-1) && (layerNum>0))
    {
        NodeLayer &hi = _layers[layerNum+1];
        NodeLayer &lo = _layers[layerNum-1];
        WeightLayer &wlo = _weights[layerNum-1];
        WeightLayer &whi = _weights[layerNum];

        cuv::prod(L.act, whi.mat, hi.act, 'n', 'n');
        cuv::matrix_plus_col(L.act, whi.bias_lo);

        cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> tmp = L.act.copy();
        cuv::prod(L.act, wlo.mat, lo.act, 't', 'n');
        cuv::matrix_plus_col(L.act, wlo.bias_hi);

        // add parts from above/below
        cuv::apply_binary_functor(L.act, tmp, cuv::BF_AXPBY, 0.5, 0.5); // computes x = a * x + b * y
        tmp.dealloc();

        L.nonlinearity();

        if (sample)
        {
            L.sample();
        }
    }
}

void CRBMStack::pcdStep(unsigned int layerNum)
{
    // _layers[layerNum].switchToPChain();
    // _layers[layerNum+1].switchToPChain();

    // upPass(layerNum, true);
    // downPass(layerNum+1, true);
    // _weights[layerNum].updateGradientNeg(_layers[layerNum], _layers[layerNum+1], _cfg.batchsize);
    
    // _layers[layerNum].switchToOrg();
    // _layers[layerNum+1].switchToOrg();

    std::cerr << "PCD not fit for training CRBMs" << std::endl;
}

void CRBMStack::cdnStep(unsigned int layerNum)
{
    for (unsigned int k=0; k<_cfg.kSteps; ++k)
    {
        upPass(layerNum, true);
        downPass(layerNum+1, true);
    }

    upPass(layerNum, true);
    _weights[layerNum].updateGradientNeg(_layers[layerNum], _layers[layerNum+1], _cfg.batchsize);
}

void CRBMStack::trainLayer(MinibatchProvider &mbp, unsigned int iterStart, unsigned int iterMax, unsigned int layerNum)
{
    // if (_cfg.cd_type == CDType::pcd)
    // {
    //     mbp.getMinibatch(_cfg.batchsize, _layers[layerNum].pChain);
    // }

    _weights[layerNum].allocUpdateMatrix();

    for (unsigned int iter = iterStart; iter<iterMax; ++iter)
    {
        _current_iter = iter;

        // new learnrate if schedule
        float learnRate = getLearnrate(iter, iterMax);

        // positive phase
        mbp.getMinibatch(_cfg.batchsize, _layers[layerNum].act, _weights[layerNum].past_visible);
        upPass(layerNum, false);
        _weights[layerNum].updateGradientPos(_layers[layerNum], _layers[layerNum+1]);

        // negative phase
        if (_cfg.cd_type == CDType::cdn)
        {
            cdnStep(layerNum);
        }
        // else if (_cfg.cd_type == CDType::pcd)
        // {
        //     pcdStep(layerNum);
        // }

        //update weights and biases
        _weights[layerNum].updateStep(learnRate, _cfg.cost);

        if ((iter != 0) && (iter%100==0))
        {
            printError(layerNum, iter, mbp);

            //debug
            if (_iter_callback)
            {
                _iter_callback();
            }
        }
    }

    _weights[layerNum].deallocUpdateMatrix();
}

void CRBMStack::run(unsigned int iterStart, unsigned int iterMax, MinibatchProvider &mbp)
{
    // Trains all levels of the RBM stack for itermax epochs. Lowest-level data comes from mbatch_provider.
    MinibatchProvider *minibatchProvider = &mbp;
    bool destroyLastProvider = false;

    for (unsigned int layer = 0; layer < _cfg.num_layers-1; ++layer)
    {
        if (layer+1 >= _cfg.continue_learning)
        {
            trainLayer(*minibatchProvider, iterStart, iterMax, layer);
        }

        if (layer < _cfg.num_layers-2)
        {
            MinibatchProvider *tmp = getHiddenRep(layer, minibatchProvider);
            if (destroyLastProvider) delete minibatchProvider;
            minibatchProvider = tmp;
            destroyLastProvider = true;
        }
    }

    if (destroyLastProvider) delete minibatchProvider;
}

MinibatchProvider* CRBMStack::getHiddenRep(unsigned int layerNum, MinibatchProvider *mbp)
{
    unsigned int id = 0;
    std::vector<cuv::tensor<float,cuv::host_memory_space,cuv::column_major> > mblist;

    while (mbp->getMinibatch(_cfg.batchsize, _layers[layerNum].act, _weights[layerNum].past_visible, true, id))
    {
        upPass(layerNum, false);
        mblist.push_back(_layers[layerNum+1].act); // copy happens (device->host)
        id++;
    }

    return new ListMinibatchProvider(mblist);
}

void CRBMStack::printError(unsigned int layerNum, unsigned int iter, MinibatchProvider &mbp)
{
    mbp.getMinibatch(_cfg.batchsize, _layers[layerNum].act, _weights[layerNum].past_visible, false);
    upPass(layerNum, false);
    downPass(layerNum+1, false);

    for (unsigned int l=layerNum; l>0; --l)
    {
        downPass(l, false);
    }

    std::chrono::high_resolution_clock::time_point rightNow = std::chrono::high_resolution_clock::now();
    std::chrono::duration<float> time_span = std::chrono::duration_cast<std::chrono::duration<double>>(rightNow - last_err_time);

    float err = getErr(layerNum, mbp.sampleset);
    float n = cuv::norm2(_weights[layerNum].mat);
    float timeSpent = time_span.count();
    std::cerr << "Iter: " << iter
              << " Err: " << err
              << " |W| = " << n
              << " -- "
              << " timespent: " << timeSpent << " -- "
              << (timeSpent/(_cfg.batchsize*(iter-last_err_time_iter))) << "s/img "
              << ((_cfg.batchsize*(iter-last_err_time_iter))/timeSpent) << "img/s"
              << std::endl;

    last_err_time = rightNow;
    last_err_time_iter = iter;
}

//void CRBMStack::initRealtime(cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> history)
void CRBMStack::initRealtime(MinibatchProvider &mbp)
{
    // unsigned int histSize = _weights[0].past_visible.shape(0) / _layers[0].act.shape(0);
    // // init activations in first past visible layer
    // cuv::copy(_weights[0].past_visible, history);

    // // init first visible layer activation with last frame + noise
    // cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> lastFrame(cuv::indices[cuv::index_range(0,_layers[0].shape()[0])][cuv::index_range(0,1)], _weights[0].past_visible.ptr()+(histSize-1)*_layers[0].shape()[0]);
    // cuv::copy(_layers[0].act, lastFrame);
    // cuv::add_

    mbp.getMinibatch(_cfg.batchsize, _layers[0].act, _weights[0].past_visible, false, 0);
}

void CRBMStack::stepRealtime(unsigned int layerNum, unsigned int gibbsSteps)
{
    for (unsigned int i=0; i<gibbsSteps; ++i)
    {
        upPass(layerNum, true);
        downPass(layerNum+1, true);
    }

    if (_step_callback)
    {
        _step_callback();
    }

    _weights[layerNum].passValues(_layers[layerNum]);
}

//serialization
void CRBMStack::saveLayer(unsigned int layerNum, const std::string &prefix, const std::string &suffix)
{

}

void CRBMStack::loadLayer(unsigned int layerNum, const std::string &prefix, const std::string &suffix)
{

}

//debug
void CRBMStack::setIterationCallback(std::function<void (void)> cb)
{
    _iter_callback = cb;
}

void CRBMStack::setGenerationCallback(std::function<void (void)> cb)
{
    _step_callback = cb;
}