#include "configuration.h"
#include "rbmStack.h"
#include "mnist/mnistData.h"
#include "mnist/mnistMinibatchProvider.h"

#include <cuv.hpp>
#include <boost/multi_array.hpp>

#include <iostream>

int main(void)
{
    //std::cerr.precision(std::numeric_limits<float>::max_digits10);
    cuv::initCUDA(0);
    cuv::initialize_mersenne_twister_seeds(30245);
    
    // cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> C(10,10),A(10,10),B(10,10);

    // cuv::fill(C,0);         // initialize to some defined value, not strictly necessary here
    // cuv::sequence(A);
    // cuv::sequence(B);

    // cuv::apply_binary_functor(A,B,cuv::BF_MULT);  // elementwise multiplication
    // A *= B;                             // operators also work (elementwise)
    // cuv::prod(C,A,B, 'n','t');               // matrix multiplication

    // std::cerr << A << std::endl;

    // //cuv::apply_scalar_functor(A, cuv::SF_MULT, 0.3);

    // //std::cerr << A << std::endl;

    // //cuv::initialize_mersenne_twister_seeds(30245);

    // cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> mat(5, 5);
    // cuv::fill(mat, 0);
    // cuv::add_rnd_normal(mat);

    // //std::cerr << mat << std::endl;

    // cuv::fill(mat, 0);
    // cuv::add_rnd_normal(mat);

    //std::cerr << mat << std::endl;

    // Configuration cfg;
    // MNISTData mnist(cfg, "../../DLTests/data/mnist");
    // cfg.batchsize = 20;
    // cfg.l_size.push_back(cfg.px*cfg.py*cfg.maps_bottom);
    // cfg.l_size.push_back(200);
    // cfg.utype.push_back(UnitType::binary);
    // cfg.utype.push_back(UnitType::binary);
    // cfg.num_layers = cfg.l_size.size();

    // mnist.normalize();
    //TODO shuffle

    // std::cerr << mnist.data.size() << std::endl;
    // std::cerr << mnist.data.shape()[0] << " " << mnist.data.shape()[1] << std::endl;

    //cuv::tensor_view<float,cuv::dev_memory_space,cuv::column_major>::type myview = myarray[ indices[range(0,2)][1][range(0,4,2)] ];

    //can only slice column-wise
    //cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> testCut(cuv::indices[cuv::index_range(0,mnist.data.shape()[0])][cuv::index_range(0,3)], mnist.data.ptr());
    // std::cerr << A.shape()[0] << " " << A.shape()[1] << std::endl;
    // cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> testCut(cuv::indices[cuv::index_range(0,A.shape()[0])][cuv::index_range(0,3)], A.ptr()+2*A.shape()[0]);

    // cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> test(10,3);

    // cuv::copy(test, testCut);

    // std::cerr << testCut << std::endl;
    // std::cerr << test << std::endl;

    // RBMStack rbm(cfg);

    // MNISTMinibatchProvider mbp(mnist);
    // rbm.run(0, 100000, mbp);

    // Configuration cfg;
    // MNISTData mnist(cfg, "../../DLTests/data/mnist");
    // std::cerr << "Normalizing...";
    // mnist.normalize();
    // std::cerr << " done" << std::endl;
    // std::cerr << "Shuffling...";
    // //mnist.shuffle();
    // std::cerr << " done" << std::endl;
    // //cfg.batchsize = 20;
    // cfg.l_size.push_back(cfg.px*cfg.py*cfg.maps_bottom);
    // cfg.utype.push_back(UnitType::binary);
    // for (int i=0; i<2; ++i)
    // {
    //     cfg.l_size.push_back(200);
    //     cfg.utype.push_back(UnitType::binary);
    // }

    // cfg.num_layers = cfg.l_size.size();
    
    // RBMStack rbm(cfg);
    // MNISTMinibatchProvider mbp(mnist);
    // rbm.run(0, 10000, mbp);

    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> A(10,10);
    cuv::sequence(A);
    std::cerr << A << std::endl;

    //can only slice column-wise
    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> testCut(cuv::indices[cuv::index_range(0,A.shape()[0]*3)][cuv::index_range(0,2)], A.ptr()+2*A.shape()[0]);
    std::cerr << testCut.shape()[0] << " " << testCut.shape()[1] << std::endl;
    std::cerr << testCut << std::endl;
}