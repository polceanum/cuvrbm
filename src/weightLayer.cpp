#include "weightLayer.h"

#include <cmath>
#include <cuv/tensor_ops/rprop.hpp>
#include <thrust/transform.h>
#include <thrust/functional.h>
#include <thrust/device_vector.h>
#include <thrust/sequence.h>
#include <thrust/fill.h>

WeightLayer::WeightLayer(const NodeLayer &layer1, const NodeLayer &layer2, const Configuration &cfg, unsigned int layerNum) :
    _cfg(cfg)
{
    mat = cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>(layer1.size, layer2.size);
    cuv::fill(mat, 0);
    cuv::add_rnd_normal(mat);

    float fact = 1.0f;
    if (layer2.unitType == UnitType::binary || layer1.unitType == UnitType::binary)
    {
        // the 0.5 stems from the fact that our upper layer has activation 0.5 on average, not 0, if we use binary hidden units.
        fact = 0.5f;
    }
    mat *= (float)(fact / sqrt(std::max(layer1.size, layer2.size)));

    allocBias(layer1, layer2);
}

WeightLayer::~WeightLayer()
{
    
}

void WeightLayer::downPass(NodeLayer &layer1, const NodeLayer &layer2, bool sample) // modifies lower layer
{
    cuv::prod(layer1.act, mat, layer2.act, 'n', 'n');
    cuv::matrix_plus_col(layer1.act, bias_lo);
    layer1.postUpdate(sample);
}

void WeightLayer::upPass(const NodeLayer &layer1, NodeLayer &layer2, bool sample) // modifies upper layer
{
    cuv::prod(layer2.act, mat, layer1.act, 't', 'n');
    cuv::matrix_plus_col(layer2.act, bias_hi);
    layer2.postUpdate(sample);
}

void WeightLayer::upPassDrop(const NodeLayer &layer1, NodeLayer &layer2, bool sample, bool pcdPass) // modifies upper layer
{
    cuv::prod(layer2.act, mat, layer1.act, 't', 'n');
    cuv::matrix_plus_col(layer2.act, bias_hi);

    layer2.postUpdate(sample);

    //-------------------------//
    // if (_cfg.use_experimental_sf)
    // {
    //     cuv::tensor<float,cuv::host_memory_space,cuv::column_major> host_act = layer2.act;
    //     for (unsigned int i=0; i<host_act.shape(1); ++i)
    //     {
    //         unsigned int sum = 0;
    //         for (unsigned int j=0; j<host_act.shape(0); ++j)
    //         {
    //             sum += host_act(j,i);
    //         }
    //         act_hist[sum]++;
    //     }
    // }
    // static int meh = 0;
    // if (meh%1000 == 0)
    // {
    //     for(int i=0; i<act_hist.size(); ++i)
    //     {
    //         //std::cerr << "(" << it->first << "): " << it->second << " ";
    //         std::cerr << act_hist[i] << " ";
    //         // iterator->first = key
    //         // iterator->second = value
    //         // Repeat if you also want to iterate through the second map.
    //     }
    //     std::cerr << std::endl;
    // }
    // meh++;
    //-------------------------//

    if (_cfg.use_dropout)
    {
        // refill random only if not pcd step (to keep the same dropout)
        if (!pcdPass)
        {
            cuv::fill(dropout_hi, 0.5);
            cuv::rnd_binarize(dropout_hi);
        }

        //cuv::apply_binary_functor(layer1.act, dropout_lo, cuv::BF_MULT);
        
        // oh gawd why
        cuv::apply_binary_functor(
            *reinterpret_cast<cuv::tensor<float, cuv::dev_memory_space, cuv::row_major>* >(const_cast<cuv::tensor<float, cuv::dev_memory_space, cuv::column_major>* >(&layer2.act)),
            *reinterpret_cast<const cuv::tensor<float, cuv::dev_memory_space, cuv::row_major>*>(&dropout_hi),
            cuv::BF_MULT);

        // static unsigned int testReset = 0;
        // if (testReset == 100000)
        // {
        //     _cfg.use_dropout = false;
        //     _cfg.cd_type = CDType::pcd;
        // }
        // testReset++;
    }

    //testing
    if (_cfg.use_experimental_sf)
    {
        static unsigned int testReset = 0;
        //cuv::fill(sf_hi, -1.0);
        if (!pcdPass)
        {
            cuv::fill(sf_hi, -0.5);
            cuv::apply_binary_functor(sf_hi, sf_act_hi, cuv::BF_MULT);
            cuv::apply_binary_functor(sf_hi, sf_tot_hi, cuv::BF_DIV);
            cuv::apply_scalar_functor(sf_hi, cuv::SF_ADD, 1.0);
            cuv::rnd_binarize(sf_hi);

            cuv::fill(sf_hi_batch, 0.0);
            cuv::matrix_plus_col(sf_hi_batch, sf_hi);

            //std::cerr << sf_hi << std::endl;

            //cuv::apply_binary_functor(layer1.act, dropout_lo, cuv::BF_MULT);
            
            cuv::reduce_to_col(sf_hi, layer2.act, cuv::RF_ADD);

        
            cuv::apply_binary_functor(sf_act_hi, sf_hi, cuv::BF_ADD);

            cuv::apply_scalar_functor(sf_tot_hi, cuv::SF_ADD, layer2.act.shape(1));

            testReset++;
        }

        // oh gawd why
        cuv::apply_binary_functor(
            *reinterpret_cast<cuv::tensor<float, cuv::dev_memory_space, cuv::row_major>* >(const_cast<cuv::tensor<float, cuv::dev_memory_space, cuv::column_major>* >(&layer2.act)),
            *reinterpret_cast<const cuv::tensor<float, cuv::dev_memory_space, cuv::row_major>*>(&sf_hi_batch),
            cuv::BF_MULT);

        if (testReset % 1000 == 0)
        {
            fill(sf_act_hi, 0);
            fill(sf_tot_hi, 1);
            //testReset = 0;
        }

        // if (testReset == 100000)
        // {
        //     fill(sf_act_hi, 0);
        //     fill(sf_tot_hi, 1);
        //     _cfg.use_experimental_sf = false;
        //     _cfg.cd_type = CDType::pcd;
        // }
    }
    

    
}

void WeightLayer::updateStep(float learnRate, float cost)
{
    // learning rate is now negative due to changes in library that for some reason flipped the sign in the learn_step_weight_decay functor
    cuv::learn_step_weight_decay(mat, _W_tmp, -learnRate, cost); // W += learnRate(dW - cost*W)
    cuv::learn_step_weight_decay(bias_lo, _blo_tmp, -learnRate, cost); // b_lo += learnRate(db_lo - cost*b_lo)
    cuv::learn_step_weight_decay(bias_hi, _bhi_tmp, -learnRate, cost); // b_hi += learnRate(db_hi - cost*b_hi)
}

void WeightLayer::updateGradientNeg(const NodeLayer &layer1, const NodeLayer &layer2, unsigned int batchSize)
{
    cuv::prod(_W_tmp, layer1.act, layer2.act, 'n', 't', -1.f/batchSize, 1.f/batchSize);
    cuv::reduce_to_col(_blo_tmp, layer1.act, cuv::RF_ADD, -1.f/batchSize, 1.f/batchSize);
    cuv::reduce_to_col(_bhi_tmp, layer2.act, cuv::RF_ADD, -1.f/batchSize, 1.f/batchSize);
}

void WeightLayer::updateGradientPos(const NodeLayer &layer1, const NodeLayer &layer2)
{
    cuv::prod(_W_tmp, layer1.act, layer2.act, 'n', 't');
    cuv::reduce_to_col(_blo_tmp, layer1.act);
    cuv::reduce_to_col(_bhi_tmp, layer2.act);
}

// gpu memory management
void WeightLayer::allocBias(const NodeLayer &layer1, const NodeLayer &layer2)
{
    bias_lo = cuv::tensor<float,cuv::dev_memory_space>(layer1.size);
    bias_hi = cuv::tensor<float,cuv::dev_memory_space>(layer2.size);
    cuv::fill(bias_lo, 0);
    cuv::fill(bias_hi, 0);

    if (_cfg.use_dropout)
    {
        dropout_lo = cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>(layer1.act.shape());
        dropout_hi = cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>(layer2.act.shape());
    }

    if (_cfg.use_experimental_sf)
    {
        sf_hi = cuv::tensor<float,cuv::dev_memory_space>(layer2.act.shape(0));
        fill(sf_hi, 0);
        sf_hi_batch = cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>(layer2.act.shape());
        fill(sf_hi_batch, 0);
        sf_act_hi = cuv::tensor<float,cuv::dev_memory_space>(layer2.act.shape(0));
        fill(sf_act_hi, 0);
        sf_tot_hi = cuv::tensor<float,cuv::dev_memory_space>(layer2.act.shape(0));
        fill(sf_tot_hi, 1);

        for (int i=0; i<layer2.act.shape(0)+1; ++i)
        {
            act_hist.push_back(0);
        }
    }
}

void WeightLayer::allocUpdateMatrix()
{
    _W_tmp = cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>(mat.shape());
    cuv::fill(_W_tmp, 0);
    _blo_tmp = cuv::tensor<float,cuv::dev_memory_space>(bias_lo.shape());
    _bhi_tmp = cuv::tensor<float,cuv::dev_memory_space>(bias_hi.shape());
    cuv::fill(_blo_tmp, 0);
    cuv::fill(_bhi_tmp, 0);
}

void WeightLayer::deallocUpdateMatrix()
{
    cuv::safeThreadSync();
    _W_tmp.dealloc();
    _blo_tmp.dealloc();
    _bhi_tmp.dealloc();
}

// serialization
void WeightLayer::save(const std::string &prefix, const std::string &postfix)
{
    //meh
}

void WeightLayer::load(const std::string &prefix, const std::string &postfix)
{
    //meh
}