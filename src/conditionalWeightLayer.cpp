#include "conditionalWeightLayer.h"

#include <cmath>
#include <cuv/tensor_ops/rprop.hpp>

ConditionalWeightLayer::ConditionalWeightLayer(const NodeLayer &layer1, const NodeLayer &layer2, const Configuration &cfg, unsigned int layerNum, unsigned int delay) : WeightLayer(layer1, layer2, cfg, layerNum)
{
    past_visible = cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>(layer1.size*delay, cfg.batchsize);
    std::cerr << layer1.size*delay << " " << layer1.size << std::endl;
    cond_a = cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>(layer1.size*delay, layer1.size);
    cond_b = cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>(layer1.size*delay, layer2.size);
    cuv::fill(cond_a, 0);
    cuv::fill(cond_b, 0);
    cuv::add_rnd_normal(cond_a);
    cuv::add_rnd_normal(cond_b);

    float fact = 1.0f;
    if (layer2.unitType == UnitType::binary || layer1.unitType == UnitType::binary)
    {
        // the 0.5 stems from the fact that our upper layer has activation 0.5 on average, not 0, if we use binary hidden units.
        fact = 0.5f;
    }
    cond_a *= (float)(fact / sqrt(std::max(layer1.size*delay, layer1.size)));
    cond_b *= (float)(fact / sqrt(std::max(layer1.size*delay, layer2.size)));
}

ConditionalWeightLayer::~ConditionalWeightLayer()
{
    
}

void ConditionalWeightLayer::downPass(NodeLayer &layer1, const NodeLayer &layer2, bool sample) // modifies lower layer
{
    cuv::prod(layer1.act, mat, layer2.act, 'n', 'n');
    cuv::prod(layer1.act, cond_a, past_visible, 't', 'n', 1.f, 1.f); // l1act += cond_a'*l2act
    cuv::matrix_plus_col(layer1.act, bias_lo);
    layer1.postUpdate(sample);
}

void ConditionalWeightLayer::upPass(const NodeLayer &layer1, NodeLayer &layer2, bool sample) // modifies upper layer
{
    cuv::prod(layer2.act, mat, layer1.act, 't', 'n');
    cuv::prod(layer2.act, cond_b, past_visible, 't', 'n', 1.f, 1.f); // l2act += cond_b'*past_visible
    cuv::matrix_plus_col(layer2.act, bias_hi);
    layer2.postUpdate(sample);
}

void ConditionalWeightLayer::updateStep(float learnRate, float cost)
{
    // learning rate is now negative due to changes in library that for some reason flipped the sign in the learn_step_weight_decay functor
    WeightLayer::updateStep(learnRate, cost);
    cuv::learn_step_weight_decay(cond_a, _a_tmp, -learnRate, cost);
    cuv::learn_step_weight_decay(cond_b, _b_tmp, -learnRate, cost);
}

void ConditionalWeightLayer::updateGradientNeg(const NodeLayer &layer1, const NodeLayer &layer2, unsigned int batchSize)
{
    WeightLayer::updateGradientNeg(layer1, layer2, batchSize);
    cuv::prod(_a_tmp, past_visible, layer1.act, 'n', 't', -1.f/batchSize, 1.f/batchSize);
    cuv::prod(_b_tmp, past_visible, layer2.act, 'n', 't', -1.f/batchSize, 1.f/batchSize);
}

void ConditionalWeightLayer::updateGradientPos(const NodeLayer &layer1, const NodeLayer &layer2)
{
    WeightLayer::updateGradientPos(layer1, layer2);
    cuv::prod(_a_tmp, past_visible, layer1.act, 'n', 't');
    cuv::prod(_b_tmp, past_visible, layer2.act, 'n', 't');
}

void ConditionalWeightLayer::passValues(NodeLayer &layer1)
{
    unsigned int histSize = past_visible.shape(0) / layer1.act.shape(0);
    unsigned int batchsize = layer1.act.shape(1);

    for (unsigned int b=0; b<batchsize; ++b)
    {
        if (histSize > 1)
        {
            cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> leftHist(cuv::indices[cuv::index_range(0,layer1.act.shape()[0]*(histSize-1))][cuv::index_range(0,1)], past_visible.ptr()+b*layer1.act.shape()[0]);
            //std::cerr << (id+b) << " " << dataset.shape(0) << " " << histdata.shape(0) << " " << histdata.shape(1) << std::endl;

            cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> rightHist(cuv::indices[cuv::index_range(0,layer1.act.shape()[0]*(histSize-1))][cuv::index_range(0,1)], past_visible.ptr()+b*layer1.act.shape()[0]+layer1.act.shape()[0]);
            //std::cerr << (b) << " " << hist_layer.shape(0) << " " << histblock.shape(0) << " " << histblock.shape(1) << std::endl;
            cuv::copy(leftHist, rightHist);
        }

        cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> newHist(cuv::indices[cuv::index_range(0,layer1.act.shape()[0])][cuv::index_range(0,1)], past_visible.ptr()+b*layer1.act.shape()[0]+layer1.act.shape()[0]*(histSize-1));
        cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> thisAct(cuv::indices[cuv::index_range(0,layer1.act.shape()[0])][cuv::index_range(0,1)], layer1.act.ptr()+b*layer1.act.shape()[0]);
        cuv::copy(newHist, thisAct);
    }

    cuv::add_rnd_normal(layer1.act, 0.01f);
}

// gpu memory management
void ConditionalWeightLayer::allocUpdateMatrix()
{
    WeightLayer::allocUpdateMatrix();

    _a_tmp = cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>(cond_a.shape());
    _b_tmp = cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>(cond_b.shape());
    cuv::fill(_a_tmp, 0);
    cuv::fill(_b_tmp, 0);
}

void ConditionalWeightLayer::deallocUpdateMatrix()
{
    WeightLayer::deallocUpdateMatrix();

    _a_tmp.dealloc();
    _b_tmp.dealloc();
}
