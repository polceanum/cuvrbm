#include "configuration.h"

Configuration::Configuration() :
    batchsize(-1), // number of items in a minibatch - default is dataset-dependend
    kSteps(1), // number of CD/PCD steps
    cd_type(CDType::pcd), // variant of Contrastive Divergence (pcd,cdn,mpfl)
    cost(0.0f), // cost of weights (=weight decay)
    continue_learning(0), // whether to use loaded weights to continue learning
    maps_bottom(1),
    learnRate_sched(LearnRateSchedule::statiq), // learnrate schedule (static/linear/exponential/divide)
    learnRate(0.01f), //learnrate of weights
    dbm_minUpdates(5), // how many updates per layer before moving to next minibatch
    use_dropout(false), // Dropout method
    use_experimental_sf(false) // experimental synaptic fatigue method
{

}

Configuration::~Configuration()
{

}