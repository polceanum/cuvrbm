#include "dbm.h"

UpdateQ::UpdateQ(unsigned int numlayers) :
    numLayers(numlayers),
    dis(0, 1)
{
	gen.seed(time(0));
    // initialize d[i] to 0
    for (unsigned int i=0; i<numLayers; ++i)
    {
        d.push_back(0);
    }
}

UpdateQ::~UpdateQ()
{
    q.clear();
    d.clear();
}

void UpdateQ::push(const std::vector<unsigned int> &layers)
{
    for (unsigned int i=0; i<layers.size(); ++i)
    {
    	q.push_back(layers[i]);
    }
}

unsigned int UpdateQ::minUpdates(const std::vector<unsigned int> &excl)
{
    unsigned int min = UINT_MAX;
    for(unsigned int i=0; i<d.size(); ++i)
    {
        // if map key is not in excl list
        if (std::find(excl.begin(), excl.end(), i) == excl.end())
        {
            if (d[i] < min)
            {
                min = d[i];
            }
        }
    }
    return min;
}

unsigned int UpdateQ::pop(unsigned int firstLayer)
{
    unsigned int x = q.at((unsigned int)(dis(gen)*q.size()));
    std::vector<unsigned int> newQ;
    for (unsigned int i=0; i<q.size(); ++i)
    {
        if (q[i] != x)
        {
            newQ.push_back(q[i]);
        }
    }
    q = newQ;
    if (x > firstLayer)
    {
        q.push_back(x-1);
    }
    if (x+1 < numLayers)
    {
        q.push_back(x+1);
    }
    d[x]++;

    return x;
}

unsigned int UpdateQ::numUpdates()
{
    unsigned int sum = 0;
    for (unsigned int i=0; i<numLayers; ++i)
    {
        sum += d[i];
    }
    return sum;
}

// ----------------------------------------------------------- //

DBM::DBM(const Configuration &cfg) : RBMStack(cfg)
{
    assert(_weights.size() > 1);
}

DBM::~DBM()
{

}

void DBM::run(unsigned int iterStart, unsigned int iterMax, MinibatchProvider &mbp)
{
    RBMStack::run(iterStart, iterMax, mbp);

    trainDBM(mbp, iterMax);
}

void DBM::trainDBM(MinibatchProvider &mbp, unsigned int iterMax)
{
    // Train all layers of a RBM-Stack as a DBM using minibatches provided by mbp for iterMax minibatches

    // first some handy variables
    std::vector<unsigned int> uqInitZ;
    std::vector<unsigned int> uqInit0(1);
    uqInit0[0] = 0;
    std::vector<unsigned int> uqInit1(1);
    uqInit1[0] = 1;

    // if pcd get starting point of fantasies
    if (_cfg.cd_type == CDType::pcd)
    {
        resetPChain(mbp);
    }

    // temporary matrix to save update
    for (unsigned int i=0; i<_weights.size(); ++i)
    {
        _weights[i].allocUpdateMatrix();
    }

    // iterate over save updates
    for (unsigned int iter=0; iter<iterMax; ++iter)
    {
        // new learnrate if schedule
        float learnRate = getLearnrate(iter, iterMax);

        // positive phase
        mbp.getMinibatch(_cfg.batchsize, _layers[0].act);
        for (unsigned int layerNum; layerNum<_weights.size(); ++layerNum)
        {
            upPass(layerNum, false);
        }

        UpdateQ uqPos(_layers.size());
        uqPos.push(uqInit1); // must start w/ 0-th layer (???)
        
        while (uqPos.minUpdates(uqInit0) < _cfg.dbm_minUpdates)
        {
            unsigned int layerNum = uqPos.pop(1);
            updateLayer(layerNum, false);
        }

        for (unsigned int layerNum = 0; layerNum<_weights.size(); ++layerNum)
        {
            _weights[layerNum].updateGradientPos(_layers[layerNum], _layers[layerNum+1]);
        }

        // output stuff
        if (iter != 0 && (iter%100) == 0)
        {
            downPass(1, false);
            float err = getErr(0, mbp.sampleset);
            float n = cuv::norm2(_weights[0].mat);
            std::cerr << "Iter: " << iter
                      << " Err: " << err
                      << " |W| = " << n
                      << std::endl;

            //debug
            if (_iter_callback)
            {
            	for (unsigned int layerNum=0; layerNum<_weights.size(); ++layerNum)
            	{
            		_iter_callback(layerNum);
            	}
            }
        }

        // negative phase
        // replace layer nodes with pcd-chain or do initial uppass
        if (_cfg.cd_type == CDType::cdn)
        {
            for (unsigned int layerNum=0; layerNum<_weights.size(); ++layerNum)
            {
                upPass(layerNum, true);
            }
        }
        else
        {
            for (unsigned int layerNum=0; layerNum<_layers.size(); ++layerNum)
            {
                _layers[layerNum].switchToPChain();
            }
        }

        UpdateQ uqNeg(_layers.size());
        uqNeg.push(uqInit1);

        while (uqNeg.minUpdates(uqInitZ) < _cfg.dbm_minUpdates)
        {
            unsigned int layerNum = uqNeg.pop(0);
            updateLayer(layerNum, true);
        }

        // update gradients
        for (unsigned int layerNum=0; layerNum<_weights.size(); ++layerNum)
        {
            _weights[layerNum].updateGradientNeg(_layers[layerNum], _layers[layerNum+1], _cfg.batchsize);
        }

        // update weights and biases
        for (unsigned int i=0; i<_weights.size(); ++i)
        {
            _weights[i].updateStep(learnRate, _cfg.cost);
        }

        // put original layer back in place
        if (_cfg.cd_type == CDType::pcd)
        {
            for (int i=0; i<_layers.size(); ++i)
            {
                _layers[i].switchToOrg();
            }
        }
    }

    for (unsigned int i=0; i<_weights.size(); ++i)
    {
        _weights[i].deallocUpdateMatrix();
    }
}