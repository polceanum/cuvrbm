#include "dataset.h"

#include <algorithm>

Dataset::Dataset()
{

}

Dataset::~Dataset()
{
    
}

void Dataset::shuffle()
{
    unsigned int size = data.shape()[1]; // number of samples
    std::vector<int> v(size); // index vector
    std::iota (std::begin(v), std::end(v), 0); // Fill with 0, 1, ...

    std::shuffle(v.begin(), v.end(), std::mt19937{std::random_device{}()}); // shuffle indices

    //create permutation matrix
    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> newDataset(data.shape());
    cuv::fill(newDataset, 0);

    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> newLabelset;
    if (data_labels.ptr())
    {
        newLabelset = cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>(data_labels.shape());
        cuv::fill(newLabelset, 0);
    }

    for (unsigned int i=0; i<size; ++i)
    {
        cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> oriDataCol(cuv::indices[cuv::index_range(0,data.shape()[0])][cuv::index_range(0,1)], data.ptr()+v[i]*data.shape()[0]);
        cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> newDataCol(cuv::indices[cuv::index_range(0,newDataset.shape()[0])][cuv::index_range(0,1)], newDataset.ptr()+i*newDataset.shape()[0]);

        cuv::copy(newDataCol, oriDataCol);

        if (data_labels.ptr())
        {
            cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> oriLabelCol(cuv::indices[cuv::index_range(0,data_labels.shape()[0])][cuv::index_range(0,1)], data_labels.ptr()+v[i]*data_labels.shape()[0]);
            cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> newLabelCol(cuv::indices[cuv::index_range(0,newLabelset.shape()[0])][cuv::index_range(0,1)], newLabelset.ptr()+i*newLabelset.shape()[0]);
            
            cuv::copy(newLabelCol, oriLabelCol);
        }
    }

    data = newDataset;
    if (data_labels.ptr())
    {
        data_labels = newLabelset;
    }
}