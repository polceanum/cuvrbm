#ifndef _RGB_PCA_
#define _RGB_PCA_

#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>

#include <string>

class RgbPCA
{
public:
    RgbPCA(double retainedVariance);
    virtual ~RgbPCA();

    void init_from_images(const std::string &imgListPath);
    void init_from_video(const std::string &videoPath);

    cv::Mat& getAllTransformedData();
    unsigned int getEigenvectorSize();
    cv::Mat getImageAt(unsigned int index);
    unsigned int getDataLength();
    
    cv::Mat project(const cv::Mat &image);
    cv::Mat reconstruct(const cv::Mat &point);

protected:
    std::vector<cv::Mat> _images;
    cv::Mat _transformedData;

    unsigned int _nrOfChannels;
    unsigned int _imgRows;
    double _retainedVariance;
    double _min, _max;

    
    cv::PCA _pca;

    void run_pca();

    void read_imgList(const std::string& filename, std::vector<cv::Mat>& images);
    cv::Mat formatImagesForPCA(const std::vector<cv::Mat> &data);
    cv::Mat normalize(cv::InputArray _src);
};

#endif