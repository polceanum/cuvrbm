#ifndef _FLAME_MINIBATCH_PROVIDER_
#define _FLAME_MINIBATCH_PROVIDER_

#include "minibatchProvider.h"

class FlameMinibatchProvider : public MinibatchProvider
{
public:
    FlameMinibatchProvider(Dataset &mnist_data);
    virtual ~FlameMinibatchProvider();

    virtual bool getMinibatch(unsigned int batchsize, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &dst_layer, int id=-1, bool test=false);
    virtual bool getMinibatch(unsigned int batchsize, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &dst_layer, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &hist_layer, bool realNow=true, int id=-1);

protected:
    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> dataset;

    std::random_device rd;
    std::mt19937 gen;
    std::uniform_real_distribution<> dis;
    
};

#endif