#ifndef _FLAME_DATA_
#define _FLAME_DATA_

#include "dataset.h"

#include "rgbPCA.h"
#include "configuration.h"

class FlameData : public Dataset
{
public:
    FlameData(Configuration &cfg, const std::string path);
    virtual ~FlameData();

    void getMinibatch();

    RgbPCA& accessPCA();
    
    unsigned int nx, ny;

protected:
	void ReadPCAFlame(const std::string filename, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &data, Configuration &cfg);
    void ReadFlame(const std::string filename, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &data);

    RgbPCA _pca;
};

#endif