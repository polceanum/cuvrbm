
#include "configuration.h"
#include "rbmStack.h"
#include "crbmStack.h"
#include "flame/flameData.h"
#include "flame/flameMinibatchProvider.h"

#include <CImg.h>
#include <cuv.hpp>
#include <boost/multi_array.hpp>

#include <iostream>

int main(int argc,char **argv)
{
    cuv::initCUDA(0);
    cuv::initialize_mersenne_twister_seeds(30245);

    // cil::CImg<unsigned char> img("lena.png");              // Read in the image lena.png
    // const unsigned char valR = img(10,10,0,0);        // Read the red component at coordinates (10,10)
    // const unsigned char valG = img(10,10,0,1);        // Read the green component at coordinates (10,10)
    // const unsigned char valB = img(10,10,2);          // Read the blue component at coordinates (10,10) (Z-coordinate omitted here).
    // const unsigned char avg = (valR + valG + valB)/3; // Compute average pixel value.

    // //img(10,10,0) = img(10,10,1) = img(10,10,2) = avg; // Replace the pixel (10,10) by the average grey value.

    // cil::CImg<unsigned char> dispImg(512, 512, 1, 3);
    // cimg_forXY(dispImg,x,y) { dispImg(x,y,0,0)=0; dispImg(x,y,0,1)=0; dispImg(x,y,0,2)=0; }

    // cil::CImgDisplay main_disp(dispImg,"Hello world");
    // cil::CImgDisplay main_disp2(dispImg,"Hello world2");

    // // Loop 150 times @100ms each
    // //for(int i=0;i<150;i++)
    // double theta = 0.0;
    // while (true)
    // {
    //     const unsigned char valR = img(10,10,0,0);        // Read the red component at coordinates (10,10)
    //     const unsigned char valG = img(10,10,0,1);        // Read the green component at coordinates (10,10)
    //     const unsigned char valB = img(10,10,2);          // Read the blue component at coordinates (10,10) (Z-coordinate omitted here).

    //     //std::cerr << (unsigned char)(img(0,0,0,0)*(cos(theta)+1)/2.0) << std::endl;

    //     cimg_forXY(dispImg,x,y) { dispImg(x,y,0,0)=(unsigned char)(img(x,y,0,0)*(cos(theta)+1)/2.0); dispImg(x,y,0,1)=(unsigned char)(img(x,y,0,1)*(sin(theta)+1)/2.0); dispImg(x,y,0,2)=0; }

    //     main_disp.display(dispImg);
    //     main_disp2.display(img);

    //     if(main_disp.is_closed()){break;}
    //     main_disp.wait(1);

    //     theta += 0.01;
    // }

    // ------------------------------------------------------------------------------------//

    int nr_hidden = 100;

    int neurons_per_row = nr_hidden / 10;
    int neurons_per_col = nr_hidden / neurons_per_row;

    Configuration cfg;
    FlameData flame(cfg, "../data");

    cil::CImg<unsigned char> dataImg(flame.nx, flame.ny, 1, 3);
    cimg_forXY(dataImg,x,y) { dataImg(x,y,0,0)=0; dataImg(x,y,0,1)=0; dataImg(x,y,0,2)=0; }
    cil::CImgDisplay data_disp(dataImg,"Data");
    unsigned int index = 0;
    cuv::tensor<float,cuv::host_memory_space,cuv::column_major> host_data = flame.data;

    //cuv::tensor<float,cuv::host_memory_space,cuv::column_major> crtImg(cuv::indices[cuv::index_range(0,host_data.shape()[0])][cuv::index_range(0,1)], host_data.ptr()+index*host_data.shape()[0]);
    //std::vector<cv::Mat> points = flame.tensorToPoints(crtImg);
    //cv::Mat img = flame.accessPCA().reconstruct(points);

    // cv::Mat img = flame.accessPCA().reconstruct(flame.accessPCA().project(flame.accessPCA()._images[0]));
    // std::string winName = "Reconstruction | press 'q' to quit";
    // cv::namedWindow(winName, cv::WINDOW_NORMAL);
    // cv::imshow(winName, img);

    // char key = 0;
    // while(key != 'q')
    //     key = (char)cv::waitKey();

    while (true)
    {
        cuv::tensor<float,cuv::host_memory_space,cuv::column_major> crtImg(cuv::indices[cuv::index_range(0,host_data.shape()[0])][cuv::index_range(0,1)], host_data.ptr()+index*host_data.shape()[0]);

        cv::Mat points(crtImg.shape()[1], crtImg.shape()[0], CV_32F, crtImg.ptr(), crtImg.shape()[0]*sizeof(float));

        cv::Mat img = flame.accessPCA().reconstruct(points);

        cimg_forXY(dataImg,x,y)
        {
            for (int i=0; i<img.channels(); ++i)
            {
                dataImg(x,y,0,i) = img.at<cv::Vec3b>(y, x)[img.channels()-i-1];
            }
        }

        data_disp.display(dataImg);

        if(data_disp.is_closed()){break;}
        data_disp.wait(33);

        index++;
        if (index >= 101)//host_data.shape(1))
        {
            index = 0;
        }
    }

    std::cerr << "Shuffling...";
    flame.shuffle();
    std::cerr << " done" << std::endl;
    // //cfg.batchsize = 20;
    // //cfg.l_size.push_back(cfg.px*cfg.py*cfg.maps_bottom);
    // cfg.l_size.push_back(cfg.px*cfg.py*3);
    // cfg.utype.push_back(UnitType::binary);
    // cfg.l_size.push_back(nr_hidden);
    // cfg.utype.push_back(UnitType::binary);

    // cfg.num_layers = cfg.l_size.size();

    // cfg.cd_type = CDType::cdn;

    // CRBMStack crbm(cfg,1);

    // FlameMinibatchProvider mbp(flame);
    // // rbm.run(0, 50000, mbp);

    // // while (true)
    // // {
    // //     if(main_disp.is_closed()){break;}
    // //     main_disp.wait(100);
    // // }


    //--------------------------

    //cfg.batchsize = 20;
    std::cerr << cfg.px*cfg.py*cfg.maps_bottom << std::endl;
    cfg.l_size.push_back(cfg.px*cfg.py*cfg.maps_bottom);
    cfg.utype.push_back(UnitType::gaussian);
    cfg.l_size.push_back(nr_hidden);
    cfg.utype.push_back(UnitType::binary);

    cfg.num_layers = cfg.l_size.size();

    cfg.learnRate = 0.01;

    RBMStack rbm(cfg);

    FlameMinibatchProvider mbp(flame);
    MinibatchStatistics mbs(mbp, rbm._layers[0].act);
    mbp.norm = [&](cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &x) { mbs.normalize_zmuv(x); };

    // prepare display
    //cil::CImg<unsigned char> dispImg(cfg.px*neurons_per_row, cfg.py*neurons_per_col, 1, 3);
    // cil::CImg<unsigned char> dispImg(flame.nx*neurons_per_row, flame.ny*neurons_per_col, 1, 3);
    // cimg_forXY(dispImg,x,y) { dispImg(x,y,0,0)=0; dispImg(x,y,0,1)=0; dispImg(x,y,0,2)=0; }

    cil::CImg<unsigned char> sampImg(flame.nx, flame.ny, 1, 3);
    cimg_forXY(sampImg,x,y) { sampImg(x,y,0,0)=0; sampImg(x,y,0,1)=0; sampImg(x,y,0,2)=0; }

    //cil::CImgDisplay main_disp(dispImg,"Features");
    cil::CImgDisplay samp_disp(sampImg,"Sample");

    float min = 0.0, max = 1.0, tmpMin = 0.0, tmpMax = 0.0;

    std::function<void (unsigned int)> callback = [&](unsigned int layerNum)
    {
        std::cerr << "Hello Lambda" << std::endl;

        cuv::tensor<float,cuv::host_memory_space,cuv::column_major> hostW = rbm._weights[0].mat;

        tmpMin = 0.0;
        tmpMax = 0.0;

        // unsigned int imgNr = 0;
        // for (unsigned int offsetX=0; offsetX<neurons_per_row; ++offsetX)
        // {
        //     for (unsigned int offsetY=0; offsetY<neurons_per_col; ++offsetY)
        //     {
        //         cimg_forXY(dataImg,x,y)
        //         {
        //             unsigned int wIndexX = y*cfg.px+x;
        //             unsigned int wIndexY = imgNr;

        //             if (hostW(wIndexX, wIndexY) < tmpMin) tmpMin = hostW(wIndexX, wIndexY);
        //             if (hostW(wIndexX, wIndexY) > tmpMax) tmpMax = hostW(wIndexX, wIndexY);

        //             unsigned int oIndexX = offsetX*cfg.px + x;
        //             unsigned int oIndexY = offsetY*cfg.py + y;
        //             dispImg(oIndexX,oIndexY,0,0) = (hostW(wIndexX*3+2, wIndexY) - min)/(max-min)*255;
        //             dispImg(oIndexX,oIndexY,0,1) = (hostW(wIndexX*3+1, wIndexY) - min)/(max-min)*255;
        //             dispImg(oIndexX,oIndexY,0,2) = (hostW(wIndexX*3+0, wIndexY) - min)/(max-min)*255;
        //         }
        //         imgNr++;
        //     }
        // }

        //cuv::tensor<float,cuv::host_memory_space,cuv::column_major> hostAct = rbm._layers[0].act;

        cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> actCopy = cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>(rbm._layers[0].act.shape());
        cuv::copy(actCopy, rbm._layers[0].act);

        mbs.denormalize_zmuv(actCopy);

        cuv::tensor<float,cuv::host_memory_space,cuv::column_major> hostAct = actCopy;

        cv::Mat actPoints(1, hostAct.shape()[0], CV_32F, hostAct.ptr(), hostAct.shape()[0]*sizeof(float));

        cv::Mat actImg = flame.accessPCA().reconstruct(actPoints);

        cimg_forXY(sampImg,x,y)
        {
            for (int i=0; i<actImg.channels(); ++i)
            {
                sampImg(x,y,0,i) = (float)actImg.at<cv::Vec3b>(y, x)[actImg.channels()-i-1];
            }
        }

        // cimg_forXY(dataImg,x,y)
        // {
        //     unsigned int wIndexX = y*cfg.px+x;
        //     unsigned int wIndexY = 0;

        //     sampImg(x,y,0,0) = hostAct(wIndexX*3+2, wIndexY)*255;
        //     sampImg(x,y,0,1) = hostAct(wIndexX*3+1, wIndexY)*255;
        //     sampImg(x,y,0,2) = hostAct(wIndexX*3+0, wIndexY)*255;
        // }

        // for (int offsetX=0; offsetX<neurons_per_row; ++offsetX)
        // {
        //     for (int offsetY=0; offsetY<neurons_per_col; ++offsetY)
        //     {
        //         for (int x=0; x<cfg.px; ++x)
        //         {
        //             for (int y=0; y<cfg.py; ++y)
        //             {
        //                 if (hostW((cfg.px-x-1)*cfg.py+(cfg.py-y-1), offsetX*neurons_per_col+offsetY) < tmpMin) tmpMin = hostW((cfg.px-x-1)*cfg.py+(cfg.py-y-1), offsetX*neurons_per_col+offsetY);
        //                 if (hostW((cfg.px-x-1)*cfg.py+(cfg.py-y-1), offsetX*neurons_per_col+offsetY) > tmpMax) tmpMax = hostW((cfg.px-x-1)*cfg.py+(cfg.py-y-1), offsetX*neurons_per_col+offsetY);
        //                 float val = (hostW((cfg.px-x-1)*cfg.py+(cfg.py-y-1), offsetX*neurons_per_col+offsetY)-min)/(max-min);
        //                 dispImg(offsetX*cfg.px+x,offsetY*cfg.py+y,0,0)=val*255;
        //                 dispImg(offsetX*cfg.px+x,offsetY*cfg.py+y,0,1)=val*255;
        //                 dispImg(offsetX*cfg.px+x,offsetY*cfg.py+y,0,2)=val*255;
        //             }
        //         }
        //     }
        // }
        

        min = tmpMin;
        max = tmpMax;

        //main_disp.display(dispImg);
        samp_disp.display(sampImg);
    };

    rbm.setIterationCallback(callback);

    
    // if (cfg.utype[0] == UnitType::gaussian)
    // {
    //     mbp.norm = [&](cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &x) { mbs.normalize_zmuv(x); };
    //     cfg.learnRate = 0.001;
    // }
    // else
    // {
    //     mbp.norm = [&](cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &x) { mbs.normalize_255(x); };
    // }

    rbm.run(0, 200000, mbp);

    while (true)
    {
        if(samp_disp.is_closed()){break;}
        samp_disp.wait(100);
    }

    return 0;
}