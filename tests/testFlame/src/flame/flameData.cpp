#include "flame/flameData.h"

#include <iostream>
#include <fstream>

FlameData::FlameData(Configuration &cfg, const std::string path) : Dataset(),
    _pca(RgbPCA(0.95)) //default
{
    nx = 210;
    ny = 400;

    cfg.test_batchsize = 10;

    if (cfg.batchsize == -1)
    {
        cfg.batchsize = 20;
    }

    //ReadFlame(path+"/flame_data.csv", data);
    //ReadMNIST(path+"/t10k-images.idx3-ubyte", test_data);
    //ReadMNIST(path+"/train-labels.idx1-ubyte", data_labels);
    //ReadMNIST(path+"/t10k-labels.idx1-ubyte", test_labels);

    //ReadPCAFlame(path+"/flame_input.mp4", data);
    ReadPCAFlame(path+"/flame.list", data, cfg);
}

FlameData::~FlameData()
{

}

RgbPCA& FlameData::accessPCA()
{
    return _pca;
}

void FlameData::ReadPCAFlame(const std::string filename, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &data, Configuration &cfg)
{
    std::cerr << "Loading flame data..." << std::endl;
    _pca = RgbPCA(0.99);
    //pca.init_from_video(filename);
    _pca.init_from_images(filename);

    unsigned int dataSize = _pca.getEigenvectorSize();
    unsigned int dataLength = _pca.getDataLength();

    cfg.px = dataSize;
    cfg.py = 1;
    cfg.maps_bottom = 1;

    cv::Mat cvData = _pca.getAllTransformedData().clone();

    // cvData is in row-major but luckly enough we have data points on rows, so we can directly convert to column-major for cuv
    cuv::tensor<float,cuv::host_memory_space,cuv::column_major> host_data(cuv::indices[cuv::index_range(0,dataSize)][cuv::index_range(0,dataLength)], (float*)cvData.data);

    std::cerr << "Transfering to device... ";
    data = host_data; //transfer to device
    std::cerr << "done" << std::endl;
}

void FlameData::ReadFlame(const std::string filename, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &data)
{
    cuv::tensor<float,cuv::host_memory_space,cuv::column_major> host_data = cuv::tensor<float,cuv::host_memory_space,cuv::column_major>(nx*ny*3, 100);

    std::cerr << "Loading flame data..." << std::endl;
    std::ifstream file ( filename ); // declare file stream: http://www.cplusplus.com/reference/iostream/ifstream/
    std::string value;
    int imgNr = 0;
    int pixelNr = 0;
    float a;
    while ( file >> a )
    {
        host_data(pixelNr, imgNr) = a;

        pixelNr++;
        if (pixelNr % (nx*ny*3) == 0)
        {
            pixelNr = 0;
            imgNr++;
        }
    }

    std::cerr << imgNr << " " << pixelNr << std::endl;

    std::cerr << "Transfering to device... ";
    data = host_data; //transfer to device
    std::cerr << "done" << std::endl;
}