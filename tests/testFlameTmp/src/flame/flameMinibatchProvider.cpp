#include "flame/flameMinibatchProvider.h"

FlameMinibatchProvider::FlameMinibatchProvider(Dataset &flame_data) : MinibatchProvider(),
    gen(rd()),
    dis(0, 1)
{
    dataset = flame_data.data;
}

FlameMinibatchProvider::~FlameMinibatchProvider()
{

}

bool FlameMinibatchProvider::getMinibatch(unsigned int batchsize, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &dst_layer, int id, bool test)
{
    //assumes data is in columns (several columns = different samples)
    if (id < 0)
    {
        pos += batchsize;
        pos %= dataset.shape()[1]; // number of samples
        if (dataset.shape()[1] < pos+batchsize-1)
        {
            pos = 0;
        }
        id = pos;
    }
    else
    {
        id = id*batchsize;
    }

    if (dataset.shape()[1] < id+batchsize)
    {
        return false;
    }

    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> minibatch(cuv::indices[cuv::index_range(0,dataset.shape()[0])][cuv::index_range(0,batchsize)], dataset.ptr()+id*dataset.shape()[0]);

    setMinibatch(dst_layer, minibatch);

    return true;
}

bool FlameMinibatchProvider::getMinibatch(unsigned int batchsize, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &dst_layer, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &hist_layer, bool realNow, int id)
{
    unsigned int histSize = hist_layer.shape(0) / dst_layer.shape(0);
    //assumes data is in columns (several columns = different samples)
    // if (id < 0)
    // {
    //     pos += batchsize;
    //     pos %= dataset.shape()[1]; // number of samples
    //     if (dataset.shape()[1] < pos+batchsize+histSize-1)
    //     {
    //         pos = 0;
    //     }
    //     id = pos;
    // }
    // else
    // {
    //     id = id*batchsize;
    // }

    // if (dataset.shape()[1] < id+batchsize+histSize)
    // {
    //     return false;
    // }

    unsigned int lastIdRand[batchsize];
    for (unsigned int b=0; b<batchsize; ++b)
    {
        unsigned int idRand = (unsigned int)(dis(gen)*(dataset.shape()[1]-histSize-1));

        //cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> histdata(cuv::indices[cuv::index_range(0,dataset.shape()[0]*histSize)][cuv::index_range(0,1)], dataset.ptr()+(id+b)*dataset.shape()[0]);
        cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> histdata(cuv::indices[cuv::index_range(0,dataset.shape()[0]*histSize)][cuv::index_range(0,1)], dataset.ptr()+(idRand)*dataset.shape()[0]);
        

        cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> histblock(cuv::indices[cuv::index_range(0,hist_layer.shape()[0])][cuv::index_range(0,1)], hist_layer.ptr()+b*hist_layer.shape()[0]);
        //std::cerr << (b) << " " << hist_layer.shape(0) << " " << histblock.shape(0) << " " << histblock.shape(1) << std::endl;
        cuv::copy(histblock, histdata);

        if (norm)
        {
            //normalize chunks of histSize length since our normalization only works for the size of the dst_layer
            for (int i=0; i<histSize; ++i)
            {
                cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> histblockChunk(cuv::indices[cuv::index_range(0,dst_layer.shape()[0])][cuv::index_range(0,1)], histblock.ptr()+i*dst_layer.shape()[0]);
                norm(histblockChunk);
            }
            
        }

        // for (int i=0; i<histSize; ++i)
        // {
        //     std::cerr << histblock(120000+i*252000,0) << " ";
        // }
        // std::cerr << std::endl;

        cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> minibatch(cuv::indices[cuv::index_range(0,dataset.shape()[0])][cuv::index_range(0,1)], dataset.ptr()+(idRand+histSize)*dataset.shape()[0]);

        cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> dstLayerSlice(cuv::indices[cuv::index_range(0,dataset.shape()[0])][cuv::index_range(0,1)], dst_layer.ptr()+b*dst_layer.shape()[0]);

        cuv::copy(dstLayerSlice, minibatch);
        
        lastIdRand[b] = idRand;
    }

    setMinibatch(dst_layer, dst_layer); //clever hack

    if (!realNow)
    {
        for (unsigned int b=0; b<batchsize; ++b)
        {
            //put last frame with noise instead
            cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> minibatch(cuv::indices[cuv::index_range(0,dataset.shape()[0])][cuv::index_range(0,1)], dataset.ptr()+(lastIdRand[b]+histSize-1)*dataset.shape()[0]);
            cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> dstLayerSlice(cuv::indices[cuv::index_range(0,dataset.shape()[0])][cuv::index_range(0,1)], dst_layer.ptr()+b*dst_layer.shape()[0]);
            cuv::copy(dstLayerSlice, minibatch);
        }
        if (norm)
        {
            norm(dst_layer);
        }
        cuv::add_rnd_normal(dst_layer, 0.01f);
    }

    return true;
}