#include "rgbPCA.h"

#include <iostream>
#include <fstream>

RgbPCA::RgbPCA(double retainedVariance):
    _retainedVariance(retainedVariance),
    _min(0.0),
    _max(0.0)
{

}

RgbPCA::~RgbPCA()
{

}

cv::Mat RgbPCA::project(const cv::Mat &image)
{
    if (image.channels() != _nrOfChannels)
    {
        std::cerr << "Image to project does not have the same number of channels (" << image.channels() << " vs " << _nrOfChannels << ")" << std::endl;
        exit(1);
    }

    std::vector<cv::Mat> imageVecHack;
    imageVecHack.push_back(image);

    cv::Mat imgChans = formatImagesForPCA(imageVecHack);

    std::vector<cv::Mat> points;
    cv::Mat point = _pca.project(imgChans); // project into the eigenspace, thus the image becomes a "point"

    return point;
}

cv::Mat RgbPCA::reconstruct(const cv::Mat &point)
{
    cv::Mat reconstructionChan;

    reconstructionChan = _pca.backProject(point); // re-create the image from the "point"
    reconstructionChan = reconstructionChan.reshape(1, _imgRows); // reshape from a row vector into image shape
    reconstructionChan = normalize(reconstructionChan); // re-scale for displaying purposes

    // Crop the full image to that image contained by the rectangle myROI
    // Note that this doesn't copy the data
    std::vector<cv::Mat> chans(_nrOfChannels);
    for (int i=0; i<_nrOfChannels; ++i)
    {
        cv::Rect myROI(i*(reconstructionChan.cols/_nrOfChannels), 0, (reconstructionChan.cols/_nrOfChannels), reconstructionChan.rows);
        chans[i] = reconstructionChan(myROI).clone();
    }
    
    cv::Mat reconstruction;
    cv::merge(chans, reconstruction);

    return reconstruction;
}

cv::Mat& RgbPCA::getAllTransformedData()
{
    return _transformedData;
}

unsigned int RgbPCA::getEigenvectorSize()
{
    return _pca.eigenvectors.rows;
}

cv::Mat RgbPCA::getImageAt(unsigned int index)
{
    return _images[index];
}

unsigned int RgbPCA::getDataLength()
{
    return _images.size();
}

unsigned int RgbPCA::getImgRows()
{
    return _imgRows;
}

unsigned int RgbPCA::getImgCols()
{
    return _imgCols;
}

void RgbPCA::init_from_images(const std::string &imgListPath)
{
    // Read in the data. This can fail if not valid
    try
    {
        read_imgList(imgListPath, _images);
    }
    catch (cv::Exception& e)
    {
        std::cerr << "Error opening file \"" << imgListPath << "\". Reason: " << e.msg << std::endl;
        exit(1);
    }

    run_pca();
}

void RgbPCA::init_from_video(const std::string &videoPath)
{
    cv::VideoCapture cap;

    cap.open(videoPath); // open the default camera

    if(!cap.isOpened()) // check if we succeeded
    {
        exit(1);
    }

    cv::Mat frame;
    cap.read(frame);

    while (true)
    {
        cv::waitKey(30);

        cv::Mat frame;

        bool bSuccess = cap.read(frame); // read a new frame from video

        if (!bSuccess) //if not success, break loop
        {
            std::cout << "Cannot read the frame from video file" << std::endl;
            break;
        }

        // if(cv::waitKey(30) == 27) //wait for 'esc' key press for 30 ms. If 'esc' key is pressed, break loop
        // {
        //     std::cout << "esc key is pressed by user" << std::endl; 
        //     break; 
        // }

        _images.push_back(frame);
    }

    _nrOfChannels = _images[0].channels();
    _imgRows = _images[0].rows;
    _imgCols = _images[0].cols;

    run_pca();
}

// void RgbPCA::init_from_video(const std::string &videoPath)
// {
//     cv::VideoCapture cap;

//     cap.open(0); // open the default camera

//     if(!cap.isOpened()) // check if we succeeded
//     {
//         exit(1);
//     }

//     cv::Mat frame;
//     cap.read(frame);

//     for(int i=0; i<100; ++i)
//     {
//         cv::waitKey(30);

//         cv::Mat frame;

//         cap >> frame; //cap.read(frame); // read a new frame from video

//         if (frame.empty()) //if not success, break loop
//         {
//             std::cout << "Cannot read the frame from video file" << std::endl;
//             break;
//         }

//         // if(cv::waitKey(30) == 27) //wait for 'esc' key press for 30 ms. If 'esc' key is pressed, break loop
//         // {
//         //     std::cout << "esc key is pressed by user" << std::endl; 
//         //     break; 
//         // }

//         _images.push_back(frame);
//     }
    
//     _nrOfChannels = _images[0].channels();
//     _imgRows = _images[0].rows;
//     _imgCols = _images[0].cols;

//     run_pca();
// }

void RgbPCA::run_pca()
{
    // Reshape and stack images into a rowMatrix
    cv::Mat data = formatImagesForPCA(_images);

    cv::PCA pca(data, cv::Mat(), cv::PCA::DATA_AS_ROW, _retainedVariance); // trackbar is initially set here, also this is a common value for retainedVariance
    _pca = pca;

    // gether stats
    cv::Mat reconstructionChan;
    std::vector<cv::Mat> points;
    cv::Mat point = _pca.project(data);
    _transformedData = point;
    reconstructionChan = _pca.backProject(point); // re-create the image from the "point"
    reconstructionChan = reconstructionChan.reshape(1, _imgRows); // reshape from a row vector into image shape
    cv::minMaxLoc(reconstructionChan, &_min, &_max);
}

void RgbPCA::read_imgList(const std::string& filename, std::vector<cv::Mat>& images)
{
    std::ifstream file(filename.c_str(), std::ifstream::in);
    if (!file)
    {
        std::string error_message = "No valid input file was given, please check the given filename.";
        CV_Error(cv::Error::StsBadArg, error_message);
    }
    images.clear();
    std::string line;
    while (getline(file, line))
    {
        images.push_back(cv::imread(line, CV_LOAD_IMAGE_COLOR));
    }

    // Quit if there are not enough images for this demo.
    if(images.size() <= 1)
    {
        std::string error_message = "PCA needs at least 2 images to work. Please add more images to your data set!";
        CV_Error(cv::Error::StsError, error_message);
    }

    _nrOfChannels = images[0].channels();
    _imgRows = images[0].rows;
    _imgCols = _images[0].cols;
}

cv::Mat RgbPCA::formatImagesForPCA(const std::vector<cv::Mat> &data)
{
    cv::Mat dst(static_cast<int>(data.size()), data[0].rows*data[0].cols*_nrOfChannels, CV_32F);
    for(unsigned int i = 0; i < data.size(); i++)
    {
        cv::Mat conc;
        std::vector<cv::Mat> channels(_nrOfChannels);
        cv::split(data[i], channels);

        for (unsigned int chan=0; chan<_nrOfChannels; ++chan)
        {
            if (chan == 0)
            {
                conc = channels[0];
            }
            else
            {
                hconcat(conc, channels[chan], conc);
            }
        }

        cv::Mat image_row = conc.clone().reshape(1,1);
        cv::Mat row_i = dst.row(i);
        image_row.convertTo(row_i, CV_32F);
    }

    return dst;
}

cv::Mat RgbPCA::normalize(cv::InputArray _src)
{
    cv::Mat src = _src.getMat();
    // only allow one channel
    if(src.channels() != 1)
    {
        CV_Error(cv::Error::StsBadArg, "Only Matrices with one channel are supported");
    }
    // create and return normalized image
    cv::Mat dst;
    //cv::normalize(_src, dst, 0, 255, cv::NORM_MINMAX, CV_8UC1);
    dst = 255.*(src-_min)/(_max-_min);
    dst.convertTo(dst, CV_8UC1);
    return dst;
}