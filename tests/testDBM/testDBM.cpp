
#include "configuration.h"
#include "dbm.h"
#include "mnist/mnistData.h"
#include "mnist/mnistMinibatchProvider.h"

#include <CImg.h>
#include <cuv.hpp>
#include <boost/multi_array.hpp>

#include <iostream>

int main(int argc,char **argv)
{
    cuv::initCUDA(0);
    cuv::initialize_mersenne_twister_seeds(30245);

    int nr_hidden = 500;

    int neurons_per_row = nr_hidden / 10;
    int neurons_per_col = nr_hidden / neurons_per_row;

    Configuration cfg;
    MNISTData mnist(cfg, "../../../../DLTests/data/mnist");
    std::cerr << " done" << std::endl;
    std::cerr << "Shuffling...";
    //mnist.shuffle();
    std::cerr << " done" << std::endl;
    //cfg.batchsize = 20;
    cfg.l_size.push_back(cfg.px*cfg.py*cfg.maps_bottom);
    cfg.utype.push_back(UnitType::binary);
    cfg.l_size.push_back(nr_hidden);
    cfg.utype.push_back(UnitType::binary);
    cfg.l_size.push_back(nr_hidden);
    cfg.utype.push_back(UnitType::binary);

    cfg.num_layers = cfg.l_size.size();

    cfg.use_dropout = false;
    cfg.use_experimental_sf = false;
    cfg.cd_type = CDType::cdn;
    cfg.learnRate = 0.1;

    DBM dbm(cfg);

    // prepare display
    std::vector<cil::CImg<unsigned char> > weightImages;
    std::vector<cil::CImgDisplay> weightDisplays;
    for (unsigned int i=0; i<cfg.num_layers-1; ++i)
    {
        cil::CImg<unsigned char> dispImg(cfg.px*neurons_per_row, cfg.py*neurons_per_col, 1, 3);
        cimg_forXY(dispImg,x,y) { dispImg(x,y,0,0)=0; dispImg(x,y,0,1)=0; dispImg(x,y,0,2)=0; }
        weightImages.push_back(dispImg);
        cil::CImgDisplay main_disp(dispImg,("Features "+std::to_string(i)).c_str());
        weightDisplays.push_back(main_disp);
    }

    std::vector<float> min;
    std::vector<float> max;
    std::vector<float> tmpMin;
    std::vector<float> tmpMax;
    for (unsigned int i=0; i<cfg.num_layers-1; ++i)
    {
        min.push_back(0);
        max.push_back(1);
        tmpMin.push_back(0);
        tmpMax.push_back(0);
    }

    std::function<void (unsigned int)> callback = [&](unsigned int layerNum)
    {
        std::cerr << "Hello Lambda" << std::endl;

        cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> topDownMat = cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>(cfg.l_size[layerNum], cfg.l_size[layerNum+1]);
        cuv::copy(topDownMat, dbm._weights[layerNum].mat);
        for (unsigned int i=layerNum; i>0; --i)
        {
            cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> tmp(cfg.l_size[i-1], cfg.l_size[layerNum+1]);

            cuv::matrix_plus_col(topDownMat, dbm._weights[i].bias_lo);

            cuv::prod(tmp, dbm._weights[i-1].mat, topDownMat, 'n', 'n');

            topDownMat = cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>(cfg.l_size[i-1], cfg.l_size[layerNum+1]);
            cuv::copy(topDownMat, tmp);
        }
        cuv::tensor<float,cuv::host_memory_space,cuv::column_major> hostW = topDownMat;

        tmpMin[layerNum] = 0.0;
        tmpMax[layerNum] = 0.0;

        for (int offsetX=0; offsetX<neurons_per_row; ++offsetX)
        {
            for (int offsetY=0; offsetY<neurons_per_col; ++offsetY)
            {
                for (int x=0; x<cfg.px; ++x)
                {
                    for (int y=0; y<cfg.py; ++y)
                    {
                        if (hostW((cfg.px-x-1)*cfg.py+(cfg.py-y-1), offsetX*neurons_per_col+offsetY) < tmpMin[layerNum]) tmpMin[layerNum] = hostW((cfg.px-x-1)*cfg.py+(cfg.py-y-1), offsetX*neurons_per_col+offsetY);
                        if (hostW((cfg.px-x-1)*cfg.py+(cfg.py-y-1), offsetX*neurons_per_col+offsetY) > tmpMax[layerNum]) tmpMax[layerNum] = hostW((cfg.px-x-1)*cfg.py+(cfg.py-y-1), offsetX*neurons_per_col+offsetY);
                        float val = (hostW((cfg.px-x-1)*cfg.py+(cfg.py-y-1), offsetX*neurons_per_col+offsetY)-min[layerNum])/(max[layerNum]-min[layerNum]);
                        weightImages[layerNum](offsetX*cfg.px+x,offsetY*cfg.py+y,0,0)=val*255;
                        weightImages[layerNum](offsetX*cfg.px+x,offsetY*cfg.py+y,0,1)=val*255;
                        weightImages[layerNum](offsetX*cfg.px+x,offsetY*cfg.py+y,0,2)=val*255;
                    }
                }
            }
        }

        min[layerNum] = tmpMin[layerNum];
        max[layerNum] = tmpMax[layerNum];

        for (int i=0; i<weightDisplays.size(); ++i)
        {
            weightDisplays[i].display(weightImages[i]);
        }
    };

    dbm.setIterationCallback(callback);

    MNISTMinibatchProvider mbp(mnist);
    MinibatchStatistics mbs(mbp, dbm._layers[0].act);
    if (cfg.utype[0] == UnitType::gaussian)
    {
        mbp.norm = [&](cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &x) { mbs.normalize_zmuv(x); };
        cfg.learnRate = 0.001;
    }
    else
    {
        mbp.norm = [&](cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &x) { mbs.normalize_255(x); };
    }

    dbm.run(0, 30000, mbp);

    while (true)
    {
        if(weightDisplays[0].is_closed()){break;}
        weightDisplays[0].wait(100);
    }

    return 0;
}