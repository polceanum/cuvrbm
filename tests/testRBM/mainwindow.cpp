#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "mnist.h"

#include <QDebug>
#include <QDesktopWidget>
#include <QScreen>
#include <QMessageBox>
#include <QMetaEnum>

#include <iostream>
#include <ctime>
#include <thread>

MainWindow::MainWindow(RBMStack *r, QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  setGeometry(400, 300, 600, 500);

  rbm = r;

  setupQuadraticDemo(ui->customPlot);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete rbm;
}

void MainWindow::setupQuadraticDemo(QCustomPlot *customPlot)
{
    demoName = "RBM weights";

    // configure axis rect:
    customPlot->plotLayout()->clear(); // clear default axis rect so we can start from scratch
    //customPlot->setInteractions(QCP::iRangeDrag|QCP::iRangeZoom);

    QCPColorScale* cs = new QCPColorScale(customPlot);

    int rowMax = 10;
    int colMax = 10;

    for (int i=0; i<rowMax; ++i)
    {
        for (int j=0; j<colMax; ++j)
        {
            if (rowMax*i + j >= rbm->_layers[1].size) break;

            QCPAxisRect *axisRect = new QCPAxisRect(customPlot);
            for (int k=0; k<axisRect->axes().size(); ++k) axisRect->axes()[k]->setTickLabels(false);
            // axis:
            // 0 = left
            // 1 = right
            // 2 = top
            // 3 = bottom
            
            QCPColorMap* cm = new QCPColorMap(axisRect->axes()[3], axisRect->axes()[0]);

            cm->data()->setSize(28, 28); // we want the color map to have nx * ny data points
            //cm->data()->setRange(QCPRange(0, 28), QCPRange(0, 28)); // and span the coordinate range


            cs->setType(QCPAxis::atRight); // scale shall be vertical bar with tick/axis labels right (actually atRight is already the default)
            cm->setColorScale(cs); // associate the color map with the color scale
            //cs->axis()->setLabel("Strength");

            //            // set the color gradient of the color map to one of the presets:
            cm->setInterpolate(false);
            cm->setGradient(QCPColorGradient::gpNight);

            // rescale the data dimension (color) such that all data points lie in the span visualized by the color gradient:
            cm->rescaleDataRange();

            // make sure the axis rect and color scale synchronize their bottom and top margins (so they line up):
            QCPMarginGroup *marginGroup = new QCPMarginGroup(customPlot);
            axisRect->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroup);
            cs->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroup);

            _aRects.append(axisRect);
            _cMaps.append(cm);
            customPlot->plotLayout()->addElement(i, j, axisRect);
            //customPlot->plotLayout()->addElement(i, j*2+1, cs); // add it to the right of the main axis rect
        }
    }

    customPlot->plotLayout()->addElement(0, colMax, cs); // add it to the right of the main axis rect

    // setup a timer that repeatedly calls MainWindow::realtimeDataSlot:
    connect(&dataTimer, SIGNAL(timeout()), this, SLOT(realtimeDataSlot()));
    dataTimer.start(10); // Interval 0 means to refresh as fast as possible
}

void MainWindow::realtimeDataSlot()
{
    static QTime time(QTime::currentTime());
    // calculate two new data points:
    double key = time.elapsed()/1000.0; // time elapsed since start of demo, in seconds
    
    cuv::tensor<float,cuv::host_memory_space,cuv::column_major> hostW = rbm->_weights[0].mat;

    for (int i=0; i<_cMaps.size(); ++i)
    {
        // now we assign some data, by accessing the QCPColorMapData instance of the color map:
        double x, y, z;
        for (int xIndex=0; xIndex<28; ++xIndex)
        {
            for (int yIndex=0; yIndex<28; ++yIndex)
            {
                //_cMaps[i]->data()->cellToCoord(xIndex, yIndex, &x, &y);
                z = hostW(xIndex*28+yIndex, i);
                _cMaps[i]->data()->setCell((27-xIndex), yIndex, z);
            }
        }
    }
    
    for (int i=0; i<_cMaps.size(); ++i)
    {
        _cMaps[i]->rescaleDataRange(true);
    }

    ui->customPlot->replot();

    // calculate frames per second:
    static double lastFpsKey;
    static int frameCount;
    ++frameCount;
    if (key-lastFpsKey > 2) // average fps over 2 seconds
    {
        ui->statusBar->showMessage(
        QString("%1 FPS")
            .arg(frameCount/(key-lastFpsKey), 0, 'f', 0)
            , 0);
        lastFpsKey = key;
        frameCount = 0;
    }
}






























