#ifndef UTILS_H
#define UTILS_H

#include <iostream>
#include <math.h>
using namespace std;


namespace utils {
  
double uniform(double min, double max);

int binomial(int n, double p);

double sigmoid(double x);

}

#endif