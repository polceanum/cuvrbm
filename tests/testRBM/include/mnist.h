#ifndef MNIST_H
#define MNIST_H

#include <eigen3/Eigen/Geometry>

int ReverseInt(int i);

void ReadMNIST(const std::string filename, Eigen::MatrixXd &data);

#endif