#ifndef LOGISTICREGRESSION_H
#define LOGISTICREGRESSION_H

#include <eigen3/Eigen/Geometry>

class LogisticRegression {

public:
  int N;  // num of inputs
  int n_in;
  int n_out;
  //double **W;
  Eigen::MatrixXd W;
  //double *b;
  Eigen::VectorXd b;
  LogisticRegression(int, int, int);
  ~LogisticRegression();
  void train(const Eigen::VectorXd&, const Eigen::VectorXd&, double);
  void softmax(Eigen::VectorXd&);
  void predict(const Eigen::VectorXd&, Eigen::VectorXd&);
};

#endif