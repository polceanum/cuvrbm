#ifndef DBN_H
#define DBN_H

#include "rbm.h"
#include "hiddenLayer.h"
#include "logisticRegression.h"

#include <eigen3/Eigen/Geometry>

class DBN {

public:
  int N;
  int n_ins;
  int *hidden_layer_sizes;
  int n_outs;
  int n_layers;
  HiddenLayer **sigmoid_layers;
  RBM **rbm_layers;
  LogisticRegression *log_layer;
  DBN(int, int, int*, int, int);
  ~DBN();
  void pretrain(const Eigen::MatrixXd&, double, int, int);
  void finetune(const Eigen::MatrixXd&, const Eigen::MatrixXd&, double, int);
  void predict(const Eigen::VectorXd&, Eigen::VectorXd&);
};

#endif