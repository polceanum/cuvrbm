#ifndef RBM_H
#define RBM_H

#include "hiddenLayer.h"

#include <eigen3/Eigen/Geometry>

class RBM {

public:
    int N;
    int n_visible;
    int n_hidden;
    //double **W;
    //double *hbias;
    HiddenLayer* hl;
    //double *vbias;
    Eigen::VectorXd vbias;
    RBM(int, int, int);
    RBM(int, int, int, HiddenLayer*);
    RBM(int, int, int, HiddenLayer*, const Eigen::VectorXd&);
    void construct(int, int, int, HiddenLayer*, const Eigen::VectorXd&);
    ~RBM();

    Eigen::VectorXd free_energy(const Eigen::VectorXd&);
    void contrastive_divergence(const Eigen::VectorXd&, double, int);
    void sample_h_given_v(const Eigen::VectorXd&, Eigen::VectorXd&, Eigen::VectorXd&);
    void sample_v_given_h(const Eigen::VectorXd&, Eigen::VectorXd&, Eigen::VectorXd&);
    double propup(const Eigen::VectorXd&, const Eigen::VectorXd&, double);
    double propdown(const Eigen::VectorXd&, int, double);
    void gibbs_hvh(const Eigen::VectorXd&, Eigen::VectorXd&, Eigen::VectorXd&, Eigen::VectorXd&, Eigen::VectorXd&);
    void reconstruct(const Eigen::VectorXd&, double*);

    bool _intializedHiddenLayer;

    Eigen::VectorXd _v_persistentChain;
    Eigen::VectorXd _h_persistentChain;
    bool _startedPersistentChain;
};

#endif
