#ifndef HIDDENLAYER_H
#define HIDDENLAYER_H

#include <eigen3/Eigen/Geometry>

class HiddenLayer {

public:
  int N;
  int n_in;
  int n_out;
  //double **W;
  Eigen::MatrixXd W;
  //double *b;
  Eigen::VectorXd b;
  HiddenLayer(int, int, int);
  ~HiddenLayer();
  double output(const Eigen::VectorXd&, const Eigen::VectorXd&, double);
  void sample_h_given_v(const Eigen::VectorXd&, Eigen::VectorXd&);
};

#endif