#
#  QCustomPlot Plot Examples
#

QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = testRBM
TEMPLATE = app

QMAKE_CXXFLAGS += -O3 -msse2 -std=c++11 -march=native -fopenmp -D_GLIBCXX_PARALLEL -ffast-math
QMAKE_CFLAGS += -O3 -msse2 -std=c++11 -march=native -fopenmp -D_GLIBCXX_PARALLEL -ffast-math

LIBS += -fopenmp -lpthread -L/home/mike/Work/DeepLearning/CUV/build/src/cuv -lcuv -lboost_system

#QMAKE_LFLAGS +=

INCLUDEPATH += ./include ../../include /usr/local/cuda-8.0/include /home/mike/Work/DeepLearning/CUV/src

SOURCES += main.cpp\
           mainwindow.cpp \
           qcustomplot.cpp \
    ../../src/configuration.cpp \
    ../../src/nodeLayer.cpp \
    ../../src/weightLayer.cpp \
    ../../src/rbmStack.cpp \
    ../../src/dataset.cpp \
    ../../src/minibatchProvider.cpp \
    ../../src/mnist/mnistData.cpp \
    ../../src/mnist/mnistMinibatchProvider.cpp

HEADERS  += mainwindow.h \
            qcustomplot.h \
    ../../include/configuration.h \
    ../../include/nodeLayer.h \
    ../../include/weightLayer.h \
    ../../include/rbmStack.h \
    ../../include/dataset.h \
    ../../include/minibatchProvider.h \
    ../../include/mnist/mnistData.h \
    ../../include/mnist/mnistMinibatchProvider.h

FORMS    += mainwindow.ui

