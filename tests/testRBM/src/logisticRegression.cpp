#include "logisticRegression.h"

#include <iostream>
#include <string>
#include <math.h>

using namespace std;


LogisticRegression::LogisticRegression(int size, int in, int out)
{
    N = size;
    n_in = in;
    n_out = out;

    W = Eigen::MatrixXd(n_out, n_in);
    b = Eigen::VectorXd(n_out);

    int i,j;

    #pragma omp parallel for private(j)
    for(i=0; i<n_out; i++)
    {
        for(j=0; j<n_in; j++)
        {
            W(i, j) = 0;
        }
        b[i] = 0;
    }
}

LogisticRegression::~LogisticRegression()
{
}


void LogisticRegression::train(const Eigen::VectorXd& x, const Eigen::VectorXd& y, double lr)
{
    Eigen::VectorXd p_y_given_x = Eigen::VectorXd(n_out);
    double *dy = new double[n_out];

    int i,j;

    #pragma omp parallel for private(j)
    for(i=0; i<n_out; i++)
    {
        p_y_given_x[i] = 0;
        for(j=0; j<n_in; j++)
        {
            p_y_given_x[i] += W(i, j) * x[j];
        }
        p_y_given_x[i] += b[i];
    }
    softmax(p_y_given_x);

    #pragma omp parallel for private(j)
    for(i=0; i<n_out; i++)
    {
        dy[i] = y[i] - p_y_given_x[i];

        for(j=0; j<n_in; j++)
        {
            W(i, j) += lr * dy[i] * x[j] / N;
        }

        b[i] += lr * dy[i] / N;
    }
    delete[] dy;
}

void LogisticRegression::softmax(Eigen::VectorXd& x)
{
    double max = 0.0;
    double sum = 0.0;

    #pragma omp parallel for
    for(int i=0; i<n_out; i++) if(max < x[i]) max = x[i];
    
    #pragma omp parallel for reduction(+:sum)
    for(int i=0; i<n_out; i++)
    {
        x[i] = exp(x[i] - max);
        sum += x[i];
    } 

    #pragma omp parallel for
    for(int i=0; i<n_out; i++) x[i] /= sum;
}

void LogisticRegression::predict(const Eigen::VectorXd& x, Eigen::VectorXd& y)
{
    int i,j;

    #pragma omp parallel for private(j)
    for(i=0; i<n_out; i++)
    {
        y[i] = 0;
        for(j=0; j<n_in; j++)
        {
            y[i] += W(i, j) * x[j];
        }
        y[i] += b[i];
    }

    softmax(y);
}
