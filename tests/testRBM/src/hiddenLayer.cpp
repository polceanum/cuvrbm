#include "hiddenLayer.h"

#include "utils.h"

#include <iostream>
#include <math.h>

using namespace std;
using namespace utils;


HiddenLayer::HiddenLayer(int size, int in, int out)
{
    N = size;
    n_in = in;
    n_out = out;

    //W = new double*[n_out];
    W = Eigen::MatrixXd::Zero(n_out, n_in);
    //for(int i=0; i<n_out; i++) W[i] = new double[n_in];
    double a = 1.0 / n_in;

    for(int i=0; i<n_out; i++)
    {
        for(int j=0; j<n_in; j++)
        {
            W(i, j) = uniform(-a, a);
        }
    }

    //b = new double[n_out];
    b = Eigen::VectorXd::Zero(n_out);
}

HiddenLayer::~HiddenLayer()
{
    //for(int i=0; i<n_out; i++) delete W[i];
    //delete[] W;
    //delete[] b;
}

double HiddenLayer::output(const Eigen::VectorXd& input, const Eigen::VectorXd& w, double b)
{
    /*
    double linear_output = 0.0;
    for(int j=0; j<n_in; j++)
    {
        linear_output += w[j] * input[j];
    }
    linear_output += b;
    return sigmoid(linear_output);
    */
    return sigmoid(w.transpose()*input + b);
}

void HiddenLayer::sample_h_given_v(const Eigen::VectorXd& input, Eigen::VectorXd& sample)
{
    for(int i=0; i<n_out; i++)
    {
        sample[i] = binomial(1, output(input, W.row(i), b[i]));
    }
}