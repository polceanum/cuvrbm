#include "dbn.h"

#include <iostream>
#include <math.h>
#include "utils.h"


using namespace std;
using namespace utils;


// DBN
DBN::DBN(int size, int n_i, int *hls, int n_o, int n_l)
{
    int input_size;
  
    N = size;
    n_ins = n_i;
    hidden_layer_sizes = hls;
    n_outs = n_o;
    n_layers = n_l;

    sigmoid_layers = new HiddenLayer*[n_layers];
    rbm_layers = new RBM*[n_layers];

    // construct multi-layer
    for(int i=0; i<n_layers; i++)
    {
        if(i == 0)
        {
            input_size = n_ins;
        }
        else
        {
            input_size = hidden_layer_sizes[i-1];
        }

        // construct sigmoid_layer
        sigmoid_layers[i] = new HiddenLayer(N, input_size, hidden_layer_sizes[i]);

        // construct rbm_layer
        rbm_layers[i] = new RBM(N, input_size, hidden_layer_sizes[i], sigmoid_layers[i]);
    }

    // layer for output using LogisticRegression
    log_layer = new LogisticRegression(N, hidden_layer_sizes[n_layers-1], n_outs);
}

DBN::~DBN()
{
    delete log_layer;

    for(int i=0; i<n_layers; i++)
    {
        delete sigmoid_layers[i];
        delete rbm_layers[i];
    }
    delete[] sigmoid_layers;
    delete[] rbm_layers;
}


void DBN::pretrain(const Eigen::MatrixXd& input, double lr, int k, int epochs)
{
    Eigen::VectorXd layer_input;
    int prev_layer_input_size;
    Eigen::VectorXd prev_layer_input;

    Eigen::VectorXd train_X = Eigen::VectorXd(n_ins);

    for(int i=0; i<n_layers; i++) // layer-wise
    {
        for(int epoch=0; epoch<epochs; epoch++) // training epochs
        {
            if (epoch%10 == 0) std::cerr << "Pretrain epoch: " << epoch << std::endl;
            for(int n=0; n<N; n++) // input x1...xN
            {
                // initial input
                train_X = input.row(n);

                // layer input
                for(int l=0; l<=i; l++)
                {
                    if(l == 0)
                    {
                        layer_input = train_X;
                    }
                    else
                    {
                        if(l == 1) prev_layer_input_size = n_ins;
                        else prev_layer_input_size = hidden_layer_sizes[l-2];

                        prev_layer_input = layer_input;

                        layer_input = Eigen::VectorXd(hidden_layer_sizes[l - 1]);

                        sigmoid_layers[l-1]->sample_h_given_v(prev_layer_input, layer_input);
                    }
                }

                rbm_layers[i]->contrastive_divergence(layer_input, lr, k);
            }
        }
    }
}

void DBN::finetune(const Eigen::MatrixXd& input, const Eigen::MatrixXd& label, double lr, int epochs)
{
    Eigen::VectorXd layer_input;
    Eigen::VectorXd prev_layer_input;

    Eigen::VectorXd train_X = Eigen::VectorXd(n_ins);
    Eigen::VectorXd train_Y = Eigen::VectorXd(n_outs);

    for(int epoch=0; epoch<epochs; epoch++)
    {
        if (epoch % 10 == 0) std::cerr << "Finetune epoch: " << epoch << std::endl;
        for(int n=0; n<N; n++) // input x1...xN
        {
            // initial input
            train_X = input.row(n);
            train_Y = label.row(n);

            // layer input
            for(int i=0; i<n_layers; i++)
            {
                if(i == 0)
                {
                    prev_layer_input = train_X;
                }
                else
                {
                    prev_layer_input = layer_input;
                }

                layer_input = Eigen::VectorXd(hidden_layer_sizes[i]);
                sigmoid_layers[i]->sample_h_given_v(prev_layer_input, layer_input);
            }

            log_layer->train(layer_input, train_Y, lr);
        }
        // lr *= 0.95;
    }
}

void DBN::predict(const Eigen::VectorXd& x, Eigen::VectorXd& y)
{
    Eigen::VectorXd layer_input;
    Eigen::VectorXd prev_layer_input;

    double linear_output;

    prev_layer_input = Eigen::VectorXd(n_ins);
    for(int j=0; j<n_ins; j++) prev_layer_input[j] = x[j];

    // layer activation
    for(int i=0; i<n_layers; i++)
    {
        layer_input = Eigen::VectorXd(sigmoid_layers[i]->n_out);

        for(int k=0; k<sigmoid_layers[i]->n_out; k++)
        {
            linear_output = 0.0;

            for(int j=0; j<sigmoid_layers[i]->n_in; j++)
            {
                linear_output += sigmoid_layers[i]->W(k, j) * prev_layer_input[j];
            }
            linear_output += sigmoid_layers[i]->b[k];
            layer_input[k] = sigmoid(linear_output);
        }

        if(i < n_layers-1)
        {
            prev_layer_input = Eigen::VectorXd(sigmoid_layers[i]->n_out);
            for(int j=0; j<sigmoid_layers[i]->n_out; j++) prev_layer_input[j] = layer_input[j];
        }
    }
    
    y = (log_layer->W * layer_input) + log_layer->b;
  
    log_layer->softmax(y);
}
