#include "rbm.h"

#include "utils.h"

#include <iostream>
#include <math.h>
#include <omp.h>

using namespace std;
using namespace utils;

RBM::RBM(int size, int n_v, int n_h) : _intializedHiddenLayer(true)
{
    hl = 0;
    HiddenLayer* hb = new HiddenLayer(size, n_v, n_h);
    Eigen::VectorXd vb = Eigen::VectorXd::Zero(n_v);

    construct(size, n_v, n_h, hb, vb);
}

RBM::RBM(int size, int n_v, int n_h, HiddenLayer* hiddenLayer): _intializedHiddenLayer(false)
{
    hl = 0;
    Eigen::VectorXd vb = Eigen::VectorXd::Zero(n_v);

    construct(size, n_v, n_h, hiddenLayer, vb);
}

RBM::RBM(int size, int n_v, int n_h, HiddenLayer* hiddenLayer, const Eigen::VectorXd& vb) : _intializedHiddenLayer(false)
{
    construct(size, n_v, n_h, hiddenLayer, vb);
}

void RBM::construct(int size, int n_v, int n_h, HiddenLayer* hiddenLayer, const Eigen::VectorXd& vb)
{
    N = size;
    n_visible = n_v;
    n_hidden = n_h;

    hl = hiddenLayer;

    vbias = vb;

    _v_persistentChain = Eigen::VectorXd::Zero(n_visible);
    _h_persistentChain = Eigen::VectorXd::Zero(n_hidden);
    _startedPersistentChain = false;
}

RBM::~RBM()
{
    if (_intializedHiddenLayer) delete hl;
}

Eigen::VectorXd RBM::free_energy(const Eigen::VectorXd& v_sample)
{
    Eigen::VectorXd wx_b = hl->W * v_sample + hl->b;
    double vbias_term = (v_sample.cwiseProduct(vbias)).sum();
    Eigen::VectorXd hidden_term = Eigen::VectorXd::Zero(n_hidden);
    for (int i=0; i<n_hidden; ++i)
    {
        hidden_term[i] = log(1+exp(wx_b[i]));
    }
    return (-hidden_term.array() - vbias_term);
}

void RBM::contrastive_divergence(const Eigen::VectorXd& input, double lr, int k)
{
    Eigen::VectorXd ph_mean = Eigen::VectorXd(n_hidden);
    Eigen::VectorXd ph_sample = Eigen::VectorXd(n_hidden);
    Eigen::VectorXd nv_mean = Eigen::VectorXd(n_visible);
    Eigen::VectorXd nv_sample = Eigen::VectorXd(n_visible);
    Eigen::VectorXd nh_mean = Eigen::VectorXd(n_hidden);
    Eigen::VectorXd nh_sample = Eigen::VectorXd(n_hidden);

    int i,j;

    /* CD-k */
    sample_h_given_v(input, ph_mean, ph_sample);

    if (!_startedPersistentChain)
    {
        _v_persistentChain = input;
        _h_persistentChain = ph_sample;
        _startedPersistentChain = true;
    }

    //#pragma omp parallel for
    for(int step=0; step<k; step++)
    {
        if(step == 0)
        {
            gibbs_hvh(_h_persistentChain, nv_mean, nv_sample, nh_mean, nh_sample);
        }
        else
        {
            gibbs_hvh(nh_sample, nv_mean, nv_sample, nh_mean, nh_sample);
        }

        if (step == k-1)
        {
            _v_persistentChain = nv_sample; //save for next time
            _h_persistentChain = nh_sample; //save for next time
        }
    }

    //#pragma omp parallel for private(j)
    for(i=0; i<n_hidden; i++)
    {
        for(j=0; j<n_visible; j++)
        {
            hl->W(i, j) += lr * (ph_mean[i] * input[j] - nh_mean[i] * nv_sample[j]);
        }
        hl->b[i] += lr * (ph_sample[i] - nh_mean[i]);
    }

    //#pragma omp parallel for
    for(i=0; i<n_visible; i++)
    {
        vbias[i] += lr * (input[i] - nv_sample[i]);
    }
}

void RBM::sample_h_given_v(const Eigen::VectorXd& v0_sample, Eigen::VectorXd& mean, Eigen::VectorXd& sample)
{
    int i;
    //#pragma omp parallel for
    for(i=0; i<n_hidden; i++)
    {
        mean[i] = propup(v0_sample, hl->W.row(i), hl->b[i]);
        sample[i] = binomial(1, mean[i]);
    }
}

void RBM::sample_v_given_h(const Eigen::VectorXd& h0_sample, Eigen::VectorXd& mean, Eigen::VectorXd& sample)
{
    int i;
    //#pragma omp parallel for
    for(i=0; i<n_visible; i++)
    {
        mean[i] = propdown(h0_sample, i, vbias[i]);
        sample[i] = binomial(1, mean[i]);
    }
}

double RBM::propup(const Eigen::VectorXd& v, const Eigen::VectorXd& w, double b)
{
    return sigmoid((w.transpose() * v) + b);
}

double RBM::propdown(const Eigen::VectorXd& h, int i, double b)
{
    return sigmoid((hl->W.col(i).transpose() * h) + b);
}

void RBM::gibbs_hvh(const Eigen::VectorXd& h0_sample, Eigen::VectorXd& nv_mean, Eigen::VectorXd& nv_sample, Eigen::VectorXd& nh_mean, Eigen::VectorXd& nh_sample)
{
    sample_v_given_h(h0_sample, nv_mean, nv_sample);
    sample_h_given_v(nv_sample, nh_mean, nh_sample);
}

void RBM::reconstruct(const Eigen::VectorXd& v, double *reconstructed_v)
{
    Eigen::VectorXd h = Eigen::VectorXd(n_hidden);
    double pre_sigmoid_activation;

    int i;

    //#pragma omp parallel for
    for(i=0; i<n_hidden; i++)
    {
        h[i] = propup(v, hl->W.row(i), hl->b[i]);
    }

    //#pragma omp parallel for
    for(i=0; i<n_visible; i++)
    {
        reconstructed_v[i] = sigmoid((hl->W.col(i).transpose() * h) + vbias[i]);
    }
}
