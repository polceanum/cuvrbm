#include "mnist.h"

#include <iostream>
#include <fstream>

int ReverseInt(int i)
{
    unsigned char ch1, ch2, ch3, ch4;
    ch1 = i & 255;
    ch2 = (i >> 8) & 255;
    ch3 = (i >> 16) & 255;
    ch4 = (i >> 24) & 255;
    return((int)ch1 << 24) + ((int)ch2 << 16) + ((int)ch3 << 8) + ch4;
}

void ReadMNIST(const std::string filename, Eigen::MatrixXd &data)
{
    std::ifstream file(filename.c_str(), std::ios::binary);
    if (file.is_open())
    {
        int magic_number = 0;
        int number_of_images = 0;
        int n_rows = 0;
        int n_cols = 0;
        file.read((char*)&magic_number, sizeof(magic_number));
        magic_number = ReverseInt(magic_number);
        std::cerr << "magic_number = " << magic_number << std::endl;
        file.read((char*)&number_of_images, sizeof(number_of_images));
        number_of_images = ReverseInt(number_of_images);
        std::cerr << "number_of_images = " << number_of_images << std::endl;
        if (magic_number == 2051)
        {
            file.read((char*)&n_rows, sizeof(n_rows));
            n_rows = ReverseInt(n_rows);
            std::cerr << "n_rows = " << n_rows << std::endl;
            file.read((char*)&n_cols, sizeof(n_cols));
            n_cols = ReverseInt(n_cols);
            std::cerr << "n_cols = " << n_cols << std::endl;
        }
        else
        {
            n_rows = 1;
            n_cols = 1;
        }
        

        data = Eigen::MatrixXd(number_of_images, n_rows*n_cols);

        for (int i = 0; i<number_of_images; ++i)
        {
            for (int r = 0; r<n_rows; ++r)
            {
                for (int c = 0; c<n_cols; ++c)
                {
                    unsigned char temp = 0;
                    file.read((char*)&temp, sizeof(temp));
                    if (magic_number == 2051)
                    {
                        data(i, (n_rows*r) + c) = (double)temp;
                    }
                    else
                    {
                        data(i, (n_rows*r) + c) = (int)temp;
                    }
                }
            }
        }
    }
    else
    {
        std::cerr << "File not found !" << std::endl;
    }
}