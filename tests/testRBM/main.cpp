/***************************************************************************
**                                                                        **
**  QCustomPlot, an easy to use, modern plotting widget for Qt            **
**  Copyright (C) 2011-2016 Emanuel Eichhammer                            **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Emanuel Eichhammer                                   **
**  Website/Contact: http://www.qcustomplot.com/                          **
**             Date: 13.09.16                                             **
**          Version: 2.0.0-beta                                           **
****************************************************************************/

#include "mainwindow.h"

#include <QApplication>
#include <thread>
#include <iostream>
#include <ctime>
#include <limits>

#include "rbmStack.h"
#include "mnist/mnistData.h"
#include "mnist/mnistMinibatchProvider.h"

void call_from_thread();

RBMStack *prbm = NULL;
MNISTMinibatchProvider *pmbp = NULL;

int main(int argc, char *argv[])
{
    std::cerr.precision(std::numeric_limits<float>::max_digits10);
    cuv::initCUDA(0);
    cuv::initialize_mersenne_twister_seeds(30245);
    
    Configuration cfg;
    MNISTData mnist(cfg, "../../../../DLTests/data/mnist");
    std::cerr << "Normalizing...";
    mnist.normalize();
    std::cerr << " done" << std::endl;
    std::cerr << "Shuffling...";
    //mnist.shuffle();
    std::cerr << " done" << std::endl;
    //cfg.batchsize = 20;
    cfg.l_size.push_back(cfg.px*cfg.py*cfg.maps_bottom);
    cfg.l_size.push_back(200);
    cfg.utype.push_back(UnitType::binary);
    cfg.utype.push_back(UnitType::binary);
    cfg.num_layers = cfg.l_size.size();
    
    RBMStack rbm(cfg);
    MNISTMinibatchProvider mbp(mnist);
    
    prbm = &rbm;
    pmbp = &mbp;
    
    //std::thread t1(call_from_thread);

    //std::this_thread::sleep_for(std::chrono::milliseconds(1000));

    //t1.join();
    
    //return 0;

    call_from_thread();

   #if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
    QApplication::setGraphicsSystem("raster");
   #endif
    QApplication a(argc, argv);
    MainWindow w(&rbm);
    w.show();

    return a.exec();
}

void call_from_thread()
{
    prbm->run(0, 10000, *pmbp);
}
