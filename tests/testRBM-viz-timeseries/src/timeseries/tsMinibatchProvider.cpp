#include "timeseries/tsMinibatchProvider.h"

TSMinibatchProvider::TSMinibatchProvider(Dataset &ts_data, unsigned int window_size) : MinibatchProvider(),
    _window_size(window_size),
    gen(rd()),
    dis(0, 1)
{
    dataset = ts_data.data;
    // test_dataset = ts_data.test_data;
    // labelset = ts_data.data_labels;
    // test_labelset = ts_data.test_labels;
}

TSMinibatchProvider::~TSMinibatchProvider()
{

}

bool TSMinibatchProvider::getMinibatch(unsigned int batchsize, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &dst_layer, int id, bool test)
{
    if (id >= 0)
    {
        if (dataset.shape()[1]-_window_size-1 < id)
        {
            return false;
        }
    }
    

    unsigned int lastIdRand[batchsize];
    for (unsigned int b=0; b<batchsize; ++b)
    {
        unsigned int idRand = (unsigned int)(dis(gen)*(dataset.shape()[1]-_window_size-1));

        cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> histdata(cuv::indices[cuv::index_range(0,dataset.shape()[0]*_window_size)][cuv::index_range(0,1)], dataset.ptr()+(idRand)*dataset.shape()[0]);
        

        cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> histblock(cuv::indices[cuv::index_range(0,dst_layer.shape()[0])][cuv::index_range(0,1)], dst_layer.ptr()+b*dst_layer.shape()[0]);
        //std::cerr << (b) << " " << dst_layer.shape(0) << " " << histblock.shape(0) << " " << histblock.shape(1) << std::endl;
        cuv::copy(histblock, histdata);
    }

    setMinibatch(dst_layer, dst_layer); //clever hack

    return true;
}
