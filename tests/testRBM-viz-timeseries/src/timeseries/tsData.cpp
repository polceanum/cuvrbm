#include "timeseries/tsData.h"

#include <iostream>
#include <fstream>
#include <sstream>

TSData::TSData(Configuration &cfg, const std::string path, unsigned int window_size) : Dataset(),
    normalized(false)
{
    cfg.px = window_size;
    cfg.py = 1;
    cfg.test_batchsize = 20;
    cfg.maps_bottom = 1;

    if (cfg.batchsize == -1)
    {
        cfg.batchsize = 20;
    }

    ReadFile(path, data);
}

TSData::~TSData()
{

}

void TSData::ReadFile(const std::string filename, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &data)
{
    std::ifstream theFile(filename);

    std::vector<float> tmp;

    std::string line;
    std::vector<std::vector<std::string> > values;
    float d;
    while(std::getline(theFile, line))
    {
        std::istringstream os(line);
        os >> d;
        tmp.push_back(d);
    }

    cuv::tensor<float,cuv::host_memory_space,cuv::column_major> host_data = cuv::tensor<float,cuv::host_memory_space,cuv::column_major>(1, tmp.size());

    for (unsigned int i=0; i<tmp.size(); ++i)
    {
        host_data(0, i) = tmp[i];
    }

    std::cerr << "Transfering to device... ";
    data = host_data;
    std::cerr << "done" << std::endl;
}