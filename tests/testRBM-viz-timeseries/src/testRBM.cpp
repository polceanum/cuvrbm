
#include "configuration.h"
#include "rbmStack.h"
#include "timeseries/tsData.h"
#include "timeseries/tsMinibatchProvider.h"

#include <CImg.h>
#include <cuv.hpp>
#include <boost/multi_array.hpp>

#include <iostream>

int main(int argc,char **argv)
{
    cuv::initCUDA(0);
    cuv::initialize_mersenne_twister_seeds(30245);

    unsigned int window_size = 100;

    int nr_hidden = 500;

    int neurons_per_row = nr_hidden / 10;
    int neurons_per_col = nr_hidden / neurons_per_row;

    Configuration cfg;
    TSData tsData(cfg, "../data/MackeyGlass_t17.txt", window_size);
    std::cerr << " done" << std::endl;
    
    //cfg.batchsize = 20;
    cfg.l_size.push_back(cfg.px*cfg.py*cfg.maps_bottom);
    cfg.utype.push_back(UnitType::cont);
    cfg.l_size.push_back(nr_hidden);
    cfg.utype.push_back(UnitType::binary);

    cfg.num_layers = cfg.l_size.size();

    RBMStack rbm(cfg);

    // // prepare display
    // cil::CImg<unsigned char> dispImg(cfg.px*neurons_per_row, cfg.py*neurons_per_col, 1, 3);
    // cimg_forXY(dispImg,x,y) { dispImg(x,y,0,0)=0; dispImg(x,y,0,1)=0; dispImg(x,y,0,2)=0; }

    // cil::CImgDisplay main_disp(dispImg,"Features");

    // float min = 0.0, max = 1.0, tmpMin = 0.0, tmpMax = 0.0;

    std::function<void (unsigned int)> callback = [&](unsigned int layerNum)
    {
        std::cerr << "Hello Lambda" << std::endl;

        // cuv::tensor<float,cuv::host_memory_space,cuv::column_major> hostW = rbm._weights[0].mat;

        // tmpMin = 0.0;
        // tmpMax = 0.0;

        // for (int offsetX=0; offsetX<neurons_per_row; ++offsetX)
        // {
        //     for (int offsetY=0; offsetY<neurons_per_col; ++offsetY)
        //     {
        //         for (int x=0; x<cfg.px; ++x)
        //         {
        //             for (int y=0; y<cfg.py; ++y)
        //             {
        //                 if (hostW((cfg.px-x-1)*cfg.py+(cfg.py-y-1), offsetX*neurons_per_col+offsetY) < tmpMin) tmpMin = hostW((cfg.px-x-1)*cfg.py+(cfg.py-y-1), offsetX*neurons_per_col+offsetY);
        //                 if (hostW((cfg.px-x-1)*cfg.py+(cfg.py-y-1), offsetX*neurons_per_col+offsetY) > tmpMax) tmpMax = hostW((cfg.px-x-1)*cfg.py+(cfg.py-y-1), offsetX*neurons_per_col+offsetY);
        //                 float val = (hostW((cfg.px-x-1)*cfg.py+(cfg.py-y-1), offsetX*neurons_per_col+offsetY)-min)/(max-min);
        //                 dispImg(offsetX*cfg.px+x,offsetY*cfg.py+y,0,0)=val*255;
        //                 dispImg(offsetX*cfg.px+x,offsetY*cfg.py+y,0,1)=val*255;
        //                 dispImg(offsetX*cfg.px+x,offsetY*cfg.py+y,0,2)=val*255;
        //             }
        //         }
        //     }
        // }
        

        // min = tmpMin;
        // max = tmpMax;

        // main_disp.display(dispImg);
    };

    rbm.setIterationCallback(callback);

    TSMinibatchProvider mbp(tsData, window_size);
    std::cerr << "Preparing stats..." << std::endl;
    MinibatchStatistics mbs(mbp, rbm._layers[0].act);
    std::cerr << "done" << std::endl;
    if (cfg.utype[0] == UnitType::gaussian)
    {
        mbp.norm = [&](cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &x) { mbs.normalize_zmuv(x); };
        cfg.learnRate = 0.001;
    }
    else
    {
        mbp.norm = [&](cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &x) { mbs.normalize_minmax(x); };
    }

    rbm.run(0, 200000, mbp);

    // while (true)
    // {
    //     if(main_disp.is_closed()){break;}
    //     main_disp.wait(100);
    // }

    return 0;
}