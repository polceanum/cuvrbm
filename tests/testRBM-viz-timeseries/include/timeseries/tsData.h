#ifndef _TS_DATA_
#define _TS_DATA_

#include "dataset.h"

#include "configuration.h"

class TSData : public Dataset
{
public:
    TSData(Configuration &cfg, const std::string path, unsigned int window_size);
    virtual ~TSData();
    
    void getMinibatch();

protected:
    void ReadFile(const std::string filename, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &data);

    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> labelMatrix(const cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>&, int);

    bool normalized; // sanity check
};

#endif