#ifndef _TS_MINIBATCH_PROVIDER_
#define _TS_MINIBATCH_PROVIDER_

#include "minibatchProvider.h"

class TSMinibatchProvider : public MinibatchProvider
{
public:
    TSMinibatchProvider(Dataset &ts_data, unsigned int window_size);
    virtual ~TSMinibatchProvider();

    virtual bool getMinibatch(unsigned int batchsize, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &dst_layer, int id, bool test=false);

protected:
    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> dataset;
    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> test_dataset;
    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> labelset;
    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> test_labelset;

    unsigned int _window_size;

    std::random_device rd;
    std::mt19937 gen;
    std::uniform_real_distribution<> dis;
};

#endif