#ifndef _DATASET_
#define _DATASET_

#include <cuv.hpp>

class Dataset
{
public:
    Dataset();
    virtual ~Dataset();
    
    virtual void shuffle();

    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> data;
    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> test_data;
    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> data_labels;
    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> test_labels;
    
};

#endif