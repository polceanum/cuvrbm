#ifndef _NODE_LAYER_
#define _NODE_LAYER_

#include "unitType.h"

#include <cuv.hpp>

class NodeLayer
{
public:
    NodeLayer(unsigned int size, unsigned int batchSize, UnitType type);
    ~NodeLayer();

    // obtain sample based on unit type
    void sample();

    // apply sigmoid
    void nonlinearity();

    void switchToPChain();
    void switchToOrg();

    void postUpdate(bool sample);

    // gpu memory management
    void alloc();
    void dealloc();
    void allocPChain();
    void deallocPChain();

    unsigned int size;
    UnitType unitType;

    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> act;
    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> pChain;

protected:
    
    unsigned int _batchSize;

    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> _org;
};

#endif