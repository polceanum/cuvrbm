#ifndef _LOGISTIC_REGRESSION_
#define _LOGISTIC_REGRESSION_

#include "configuration.h"
#include "minibatchProvider.h"

#include <cuv.hpp>
#include <chrono>

class LogisticRegression
{
public:
    LogisticRegression(const Configuration &cfg);
    virtual ~LogisticRegression();

    void train(const cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>&, const cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>&, double);
    void predict(const cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>&, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>&);

    void run(unsigned int iterStart, unsigned int iterMax, MinibatchProvider &mbp);

    void softmax(cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>& dst);

    void allocUpdateMatrix();
    void deallocUpdateMatrix();

    void printError(unsigned int iter, MinibatchProvider &mbp);

    //debug
    void setIterationCallback(std::function<void (void)>);

    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> W;
    cuv::tensor<float,cuv::dev_memory_space> b;

protected:
    Configuration _cfg;

    unsigned int n_in;
    unsigned int n_out;
    unsigned int batch_size;

    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> p_y_given_x;
    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> tmp_W;
    cuv::tensor<float,cuv::dev_memory_space> tmp_b;

    std::chrono::high_resolution_clock::time_point last_err_time;
    unsigned int last_err_time_iter;

    // debug
    std::function<void (void)> _iter_callback;
};

#endif