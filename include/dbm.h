#ifndef _DBM_
#define _DBM_

#include "rbmStack.h"

class UpdateQ
{
public:
    UpdateQ(unsigned int numlayers);
    ~UpdateQ();

    void push(const std::vector<unsigned int> &layers);
    unsigned int minUpdates(const std::vector<unsigned int> &excl);
    unsigned int pop(unsigned int firstLayer=0);
    unsigned int numUpdates();

    std::vector<unsigned int> q;
    std::vector<unsigned int> d;
    unsigned int numLayers;

protected:
    std::mt19937 gen;
    std::uniform_real_distribution<double> dis;
};

// ----------------------------------------------------------- //

class DBM: public RBMStack
{
public:
    DBM(const Configuration &cfg);
    virtual ~DBM();

    virtual void run(unsigned int iterStart, unsigned int iterMax, MinibatchProvider &mbp);

    void trainDBM(MinibatchProvider &mbp, unsigned int iterMax);
};

#endif