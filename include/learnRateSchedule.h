#ifndef _LEARN_RATE_SCHEDULE_
#define _LEARN_RATE_SCHEDULE_

enum LearnRateSchedule
{
    statiq,      // static (learnrate)
    linear,      // learnrate * (1 - iter/itermax)
    exponential, // learnrate * exp(-10 * iter/itermax)
    divide       // if (iter<10000) learnrate; else learnrate * 1000 / (1000 + iter - 10000)
};

#endif