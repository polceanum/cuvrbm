#ifndef _CONDITIONAL_WEIGHT_LAYER_
#define _CONDITIONAL_WEIGHT_LAYER_

#include "nodeLayer.h"
#include "weightLayer.h"
#include "configuration.h"

class ConditionalWeightLayer : public WeightLayer
{
public:
    ConditionalWeightLayer(const NodeLayer &layer1, const NodeLayer &layer2, const Configuration &cfg, unsigned int layerNum, unsigned int delay);
    ~ConditionalWeightLayer();

    virtual void downPass(NodeLayer &layer1, const NodeLayer &layer2, bool sample); // modifies lower layer
    virtual void upPass(const NodeLayer &layer1, NodeLayer &layer2, bool sample); // modifies upper layer
    virtual void updateStep(float learnRate, float cost);
    virtual void updateGradientNeg(const NodeLayer &layer1, const NodeLayer &layer2, unsigned int batchSize);
    virtual void updateGradientPos(const NodeLayer &layer1, const NodeLayer &layer2);

    virtual void passValues(NodeLayer &layer1); // current visible layer will be affected

    // gpu memory management
    virtual void allocUpdateMatrix();
    virtual void deallocUpdateMatrix();

    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> past_visible;
    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> cond_a; // past visible to visible
    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> cond_b; // past visible to hidden

protected:
    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> _a_tmp;
    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> _b_tmp;
};

#endif