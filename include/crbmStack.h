#ifndef _CRBM_STACK_
#define _CRBM_STACK_

#include "configuration.h"
#include "conditionalWeightLayer.h"
#include "nodeLayer.h"
#include "minibatchProvider.h"

#include <vector>
#include <chrono>
//#include <boost/shared_ptr.hpp>

class CRBMStack
{
public:
    CRBMStack(const Configuration &cfg, unsigned int delay);
    virtual ~CRBMStack();

    float getErr(unsigned int layerNum, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &originalData);

    float getLearnrate(unsigned int iter, unsigned int iterMax);

    void upPass(unsigned int layerNum, bool sample=true);
    void downPass(unsigned int layerNum, bool sample=true);

    void updateLayer(unsigned int layerNum, bool sample=true);

    void pcdStep(unsigned int layerNum);
    void cdnStep(unsigned int layerNum);

    void trainLayer(MinibatchProvider &mbp, unsigned int iterStart, unsigned int iterMax, unsigned int layerNum);

    virtual void run(unsigned int iterStart, unsigned int iterMax, MinibatchProvider &mbp);

    MinibatchProvider* getHiddenRep(unsigned int layerNum, MinibatchProvider *mbp);

    void printError(unsigned int layerNum, unsigned int iter, MinibatchProvider &mbp);

    void initRealtime(MinibatchProvider &mbp);
    virtual void stepRealtime(unsigned int layerNum, unsigned int gibbsSteps);

    //serialization
    void saveLayer(unsigned int layerNum, const std::string &prefix, const std::string &suffix);
    void loadLayer(unsigned int layerNum, const std::string &prefix, const std::string &suffix);

    //debug
    void setIterationCallback(std::function<void (void)>);
    void setGenerationCallback(std::function<void (void)>);

//private:
    std::vector<NodeLayer> _layers;
    std::vector<ConditionalWeightLayer> _weights;

    Configuration _cfg;
    unsigned int _current_iter;

protected:
    std::chrono::high_resolution_clock::time_point last_err_time;
    unsigned int last_err_time_iter;

    // debug
    std::function<void (void)> _iter_callback;
    std::function<void (void)> _step_callback;
};

#endif