#ifndef _MINIBATCH_PROVIDER_
#define _MINIBATCH_PROVIDER_

#include "dataset.h"
#include "nodeLayer.h"

class MinibatchProvider
{
public:
    MinibatchProvider();
    virtual ~MinibatchProvider();

    void setMinibatch(cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &dst_layer, const cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &src);

    //assumes data is in columns (several columns = different samples)
    virtual bool getMinibatch(unsigned int batchsize, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &dst_layer, int id=-1, bool test=false);
    virtual bool getMinibatchWithLabel(unsigned int batchsize, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &dst_layer, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &dst_label, int id=-1, bool test=false);
    virtual bool getMinibatch(unsigned int batchsize, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &dst_layer, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &hist_layer, bool realNow=true, int id=-1);

    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> sampleset;

    std::function<void(cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>&)> norm;
protected:
    unsigned int pos;
};

// ----- //

class MinibatchStatistics
{
public:
    MinibatchStatistics(MinibatchProvider &mbp, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &act);
    ~MinibatchStatistics();

    void update_stats(cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &batch);
    void finalize_stats();

    void normalize_zmuv(cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &batch);
    void normalize_255(cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &batch);
    void normalize_180deg(cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &batch);
    void normalize_minmax(cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &batch);

    void denormalize_zmuv(cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &batch);
    void denormalize_255(cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &batch);
    void denormalize_180deg(cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &batch);
    void denormalize_minmax(cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &batch);

    int N;
    cuv::tensor<float,cuv::dev_memory_space> min;
    cuv::tensor<float,cuv::dev_memory_space> max;
    cuv::tensor<float,cuv::dev_memory_space> mean;
    cuv::tensor<float,cuv::dev_memory_space> mean2;

    cuv::tensor<float,cuv::dev_memory_space> negative_mean;
    cuv::tensor<float,cuv::dev_memory_space> negative_min;
    cuv::tensor<float,cuv::dev_memory_space> std;
    cuv::tensor<float,cuv::dev_memory_space> range;
};

// ----- //

class ListMinibatchProvider : public MinibatchProvider
{
public:
    ListMinibatchProvider();
    ListMinibatchProvider(std::vector<cuv::tensor<float,cuv::host_memory_space,cuv::column_major> > &list);
    ListMinibatchProvider(std::vector<cuv::tensor<float,cuv::host_memory_space,cuv::column_major> > &list, std::vector<cuv::tensor<float,cuv::host_memory_space,cuv::column_major> > &list_test);
    virtual ~ListMinibatchProvider();

    //assumes data is in columns (several columns = different samples)
    virtual bool getMinibatch(unsigned int batchsize, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &dst_layer, int id=-1, bool test=false);
    virtual bool getMinibatchWithLabel(unsigned int batchsize, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &dst_layer, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &dst_label, int id=-1, bool test=false);

    void setLabels(std::vector<cuv::tensor<float,cuv::host_memory_space,cuv::column_major> >& labels);
    void setTestLabels(std::vector<cuv::tensor<float,cuv::host_memory_space,cuv::column_major> >& testLabels);

protected:
    std::vector<cuv::tensor<float,cuv::host_memory_space,cuv::column_major> > dataset;
    std::vector<cuv::tensor<float,cuv::host_memory_space,cuv::column_major> > labelset;
    std::vector<cuv::tensor<float,cuv::host_memory_space,cuv::column_major> > test_dataset;
    std::vector<cuv::tensor<float,cuv::host_memory_space,cuv::column_major> > test_labelset;
};

#endif