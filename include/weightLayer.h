#ifndef _WEIGHT_LAYER_
#define _WEIGHT_LAYER_

#include "nodeLayer.h"
#include "configuration.h"

class WeightLayer
{
public:
    WeightLayer(const NodeLayer &layer1, const NodeLayer &layer2, const Configuration &cfg, unsigned int layerNum);
    ~WeightLayer();

    virtual void downPass(NodeLayer &layer1, const NodeLayer &layer2, bool sample); // modifies lower layer
    virtual void upPass(const NodeLayer &layer1, NodeLayer &layer2, bool sample); // modifies upper layer
    virtual void upPassDrop(const NodeLayer &layer1, NodeLayer &layer2, bool sample, bool pcdPass=false);
    virtual void updateStep(float learnRate, float cost);
    virtual void updateGradientNeg(const NodeLayer &layer1, const NodeLayer &layer2, unsigned int batchSize);
    virtual void updateGradientPos(const NodeLayer &layer1, const NodeLayer &layer2);

    // gpu memory management
    virtual void allocBias(const NodeLayer &layer1, const NodeLayer &layer2);
    virtual void allocUpdateMatrix();
    virtual void deallocUpdateMatrix();

    // serialization
    void save(const std::string &prefix, const std::string &postfix);
    void load(const std::string &prefix, const std::string &postfix);

    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> mat;
    cuv::tensor<float,cuv::dev_memory_space> bias_lo;
    cuv::tensor<float,cuv::dev_memory_space> bias_hi;

protected:
    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> _W_tmp;
    cuv::tensor<float,cuv::dev_memory_space> _blo_tmp;
    cuv::tensor<float,cuv::dev_memory_space> _bhi_tmp;
    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> dropout_lo;
    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> dropout_hi;
    Configuration _cfg;

public:
    cuv::tensor<float,cuv::dev_memory_space> sf_hi;
    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> sf_hi_batch;
    cuv::tensor<float,cuv::dev_memory_space> sf_act_hi;
    cuv::tensor<float,cuv::dev_memory_space> sf_tot_hi;

    std::vector<unsigned int> act_hist;
};

#endif