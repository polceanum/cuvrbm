#ifndef _CD_TYPE_
#define _CD_TYPE_

enum CDType
{
    pcd, // Persistent Contrastive Divergence
    cdn, // Constrastive Divergence with N Gibbs steps (CD-N)
    mpf  // Maximum Probability Flow
};

#endif