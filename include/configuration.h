#ifndef _CONFIGURATION_
#define _CONFIGURATION_

#include "unitType.h"
#include "cdType.h"
#include "learnRateSchedule.h"

#include <vector>

class Configuration
{
public:
    Configuration();
    ~Configuration();

    int px; // data size on x axis
    int py; // data size on y axis

    unsigned int num_layers;
    std::vector<unsigned int> l_size;
    std::vector<UnitType> utype;

    unsigned int batchsize;
    unsigned int test_batchsize;

    unsigned int maps_bottom;

    float cost;
    float learnRate;
    LearnRateSchedule learnRate_sched;

    unsigned int continue_learning;

    CDType cd_type;
    unsigned int kSteps;

    bool use_dropout;
    bool use_experimental_sf;

    unsigned int dbm_minUpdates;
};

#endif