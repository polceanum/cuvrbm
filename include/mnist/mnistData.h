#ifndef _MNIST_DATA_
#define _MNIST_DATA_

#include "dataset.h"

#include "configuration.h"

class MNISTData : public Dataset
{
public:
    MNISTData(Configuration &cfg, const std::string path);
    virtual ~MNISTData();
    
    void getMinibatch();

protected:
    int ReverseInt(int i);
    void ReadMNIST(const std::string filename, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &data);

    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> labelMatrix(const cuv::tensor<float,cuv::dev_memory_space,cuv::column_major>&, int);

    protected:
    bool normalized; // sanity check
};

#endif