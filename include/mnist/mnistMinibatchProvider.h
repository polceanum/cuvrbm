#ifndef _MNIST_MINIBATCH_PROVIDER_
#define _MNIST_MINIBATCH_PROVIDER_

#include "minibatchProvider.h"

class MNISTMinibatchProvider : public MinibatchProvider
{
public:
    MNISTMinibatchProvider(Dataset &mnist_data);
    virtual ~MNISTMinibatchProvider();

    virtual bool getMinibatch(unsigned int batchsize, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &dst_layer, int id, bool test=false);
    virtual bool getMinibatchWithLabel(unsigned int batchsize, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &dst_layer, cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> &dst_label, int id, bool test=false);

protected:
    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> dataset;
    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> test_dataset;
    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> labelset;
    cuv::tensor<float,cuv::dev_memory_space,cuv::column_major> test_labelset;
};

#endif